/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Fanjava
 */
@Controller
public class Controllerscene {

    @Autowired
    HibernateDao dao;

    @RequestMapping(value = "/listeindispo")
    public ModelAndView listeindispo() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("tray-list");
        mv.addObject("listeindisponible", dao.findAll(Indisponibiliteplateau.class));
        mv.addObject("plateau", dao.findAll(Plateau.class));
        return mv;
    }

    @RequestMapping(value = "/createindispo", method = RequestMethod.POST)
    public String createindispo(HttpServletRequest request, @ModelAttribute Plateau p) {
        Indisponibiliteplateau indisponible = new Indisponibiliteplateau();
        indisponible.setDateindispo(Date.valueOf(request.getParameter("dateindispo")));
        indisponible.setPlateauindispo(p);
        indisponible.setMotifindispo(request.getParameter("motifindispo"));
        dao.create(indisponible);
        return "index";

    }

    @RequestMapping(value = "/Listescene")
    public ModelAndView formulaireCommune() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("regions");
        mv.addObject("regions", dao.findAll(Parametrage.class));
        return mv;
    }

    @RequestMapping(value = "/")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping(value = "/listenotorder")
//    @RequestMapping(value="/home",method = RequestMethod.GET)
    public ModelAndView home(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("schedule-list");
        mv.addObject("listenotorder", dao.findorderby(Scene.class));
        return mv;
    }

    @RequestMapping(value = "/acteurs", method = RequestMethod.GET)
    public String getActeurScene(@ModelAttribute Scene scene, @ModelAttribute Plateau plateau, Model model, HttpServletRequest request) {
        System.out.println("Info Scene:".toUpperCase());
        HttpSession session = request.getSession(true);
        Film film = (Film) session.getAttribute("idfilm");
        scene.setPlateau(plateau);
        scene.setStatusplanification(1);
        scene.setFilm(film);
        dao.create(scene);
        // Create Session

        System.out.println("NOM SCENE : " + request.getParameter("nomscene"));
        // Add Scene to Session
        session.setAttribute("scene", scene);
        model.addAttribute("acteurs", dao.findAll(Acteur.class));
        return "listeacteur";
    }

    @RequestMapping(value = "/detail")
//    @RequestMapping(value="/home",method = RequestMethod.GET)
    public ModelAndView detail(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView();
        int idscene = Integer.parseInt(req.getParameter("idscene"));
        mv.setViewName("details");
        mv.addObject("detail", dao.findwhereidscene(Vueacteurscene.class, idscene));
        return mv;
    }

//    @RequestMapping(value = "/listordertrue")
////    @RequestMapping(value="/home",method = RequestMethod.GET)
//    public ModelAndView listordertrue(HttpServletRequest req) throws ParseException {
//        ModelAndView mv = new ModelAndView();
//        dao.insertionsceneExact();
//        mv.setViewName("schedule-tab-1");
//        mv.addObject("listescenetrue", dao.findAll(Listesceneordertrue.class));
//        return mv;
//    }
    // IOTY
    @RequestMapping(value = "/listordertrue")
//    @RequestMapping(value="/home",method = RequestMethod.GET)
    public ModelAndView listordertrue(HttpServletRequest req, @RequestParam("idscene[]") int scene[]) throws ParseException, Exception {

        ModelAndView mv = new ModelAndView();

        // list retour
        List<Calendrier> listesceneplanifier = new ArrayList<>();

        // Create Session
        HttpSession session = req.getSession(true);

        // Get Date Debut, Date Fin Planning        
        Date datedebut = (Date) session.getAttribute("datedebutplanifie");
        Date datefin = (Date) session.getAttribute("datefinplanifie");
        Date datedebutoriginal = datedebut;
        Date datefinoriginal = datefin;

        System.out.println("Date Debut Planning: " + datedebut);
        System.out.println("Date Fin Planning: " + datefin);

        // Get Liste Scene Selectionne
        List<Scene> sceneselectionne = new ArrayList<>();
        for (int i = 0; i < scene.length; i++) {
            System.out.println("scene selectionne: " + scene[i]);
            sceneselectionne.add(dao.findById(Scene.class, scene[i]));
        }

        for (Scene s : sceneselectionne) {
            System.out.println("Info Scene Selectionne: " + s.getIdscene() + " | " + s.getNomscene() + " | " + s.getDatescene());
        }

        //check list plateau indisponible
        List<Indisponibiliteplateau> listindispo = checkDisponibilite(req);

        // Get List Scene by Plateau,ordered by plateau,not planifier
        // List<Scene> listescenebyplateau = dao.getScenebyPlateau();
        // Sort Scene By Plateau        
        Collections.sort(sceneselectionne, new Comparator<Scene>() {
            @Override
            public int compare(Scene s1, Scene s2) {
                return s1.getPlateau().getIdplateau() - s2.getPlateau().getIdplateau();
            }
        });

        double sumdureeday = 0;
        int index = 0;

        // definition heure journée
        Time heuredebutcalendrier = Time.valueOf("08:00:00");
        Time heurefincalendrier = Time.valueOf("08:01:00");

        // Loop through listescenebyplateau      
        for (Scene s : sceneselectionne) {
            System.out.println("SCENE: ");
            System.out.println(s.getIdscene() + " | " + s.getNomscene() + " | " + s.getDatescene() + " | " + s.getHeuredebut() + " | " + s.getHeurefin() + " | " + s.getPlateau().getNomplateau() + " | " + s.getStatusplanification());

            // Get Duree by Scene (Heure Fin - Heure Debut)
            double dureescene = (s.getHeurefin().getHours() - s.getHeuredebut().getHours());
            System.out.println("duree scene en heure: " + s.getIdscene() + " : " + dureescene);

            // Add to sum duree scene by day
            sumdureeday += dureescene;
            System.out.println("Somme duree jour : " + sumdureeday);

            // Check if index is 0
            if (index == 0) {
                // Set default heure to 08 AM
                heuredebutcalendrier = Time.valueOf("08:00:00");
                heurefincalendrier = Time.valueOf(heuredebutcalendrier.toLocalTime().plusHours((int) dureescene));
            } else {
                // Else Increment by duree scene            
                heuredebutcalendrier = heurefincalendrier;
                heurefincalendrier = Time.valueOf(heurefincalendrier.toLocalTime().plusHours((int) dureescene));
            }

            // If duree > 8(nb heure dans une journee)
            if (sumdureeday > 8) {
                heuredebutcalendrier = Time.valueOf("08:00:00");
                heurefincalendrier = Time.valueOf(heuredebutcalendrier.toLocalTime().plusHours((int) dureescene));

                sumdureeday = 0;
                // Increment date form            
                LocalDate newdatedebut = datedebut.toLocalDate().plusDays(1);
                System.out.println("New Date Debut: " + newdatedebut);
                datedebut = Date.valueOf(newdatedebut);
                // Check if datenew > date fin form  
                if (datedebut.after(datefin)) {
                    // Break                
                    break;
                }

                // Add Scene to next date
                // Set Date scene to date debut from form  
                Calendrier calendrier = new Calendrier();
                calendrier.setScene(s.getIdscene());
                calendrier.setDateexact(datedebut); // new date (date debut plus one day)
                // Calculate heure starting from 8
                // Increment By duree scene                
                calendrier.setHeuredebutexact(heuredebutcalendrier);
                calendrier.setHeurefinexact(heurefincalendrier);

                Indisponibiliteplateau indi = new Indisponibiliteplateau();

                indi.setPlateauindispo(s.getPlateau());
                indi.setDateindispo(datedebut);

                // add to indisponible tray
                //dao.create(indi);
                // add to calendrier
                //dao.create(calendrier);
                listesceneplanifier.add(calendrier);

                // Update Statusplanification Scene
                s.setStatusplanification(1);
                //dao.update(s);

            } else {

                for (int i = 0; i < listindispo.size(); i++) {
                    if (datedebut.equals(listindispo.get(i).getDateindispo()) && s.getPlateau().getIdplateau() == listindispo.get(i).getPlateauindispo().getIdplateau()) {

                        List<Vcalendrierplateau> plateauIndispo = dao.findwhereidplateau(Vcalendrierplateau.class, s.getPlateau().getIdplateau());

                        int totalHeurePlateau = plateauIndispo.get(0).getHeurefinexact().getHours() - plateauIndispo.get(0).getHeuredebutexact().getHours();
                        System.out.println("TOTAL HEURE DU PLATEAU INDISPONIBLE : " + totalHeurePlateau);
                        if (totalHeurePlateau < 8) {
                            //datedebut = Date.valueOf(datedebut.toLocalDate().plusDays(1));

                        }

                    }
                }
                // Set Date scene to date debut from form  
                Calendrier calendrier = new Calendrier();
                calendrier.setScene(s.getIdscene());
                calendrier.setDateexact(datedebut);
                calendrier.setHeuredebutexact(heuredebutcalendrier);
                calendrier.setHeurefinexact(heurefincalendrier);

                Indisponibiliteplateau indi = new Indisponibiliteplateau();

                indi.setPlateauindispo(s.getPlateau());
                indi.setDateindispo(datedebut);

                // add to indisponible tray
                //dao.create(indi);
                // add to calendrier
                //dao.create(calendrier);
                listesceneplanifier.add(calendrier);

                // Update Statusplanification Scene
                s.setStatusplanification(1);
                //dao.update(s);

                datedebut = datedebutoriginal;
            }
            // Increment Index of Heure Debut,Heure Fin            
            index += 1;

        }

        // Get All Liste From Calendrier        
        //List<Listesceneordertrue> findgroupby = dao.findgroupby(Listesceneordertrue.class, "dateexact", "plateau");
        // Get Liste from calendrier by date debut, date fin form  
        List<Calendrier> listesceneplanifierbydate = new ArrayList<>();

        for (Calendrier ls : listesceneplanifier) {
            if (((ls.getDateexact().after(datedebutoriginal)) || (ls.getDateexact().equals(datedebutoriginal))) && ((ls.getDateexact().before(datefinoriginal)) || ls.getDateexact().equals(datefinoriginal))) {
                listesceneplanifierbydate.add(ls);
            }
        }

        // group by dateexacte
        Map<Date, List<Calendrier>> bookMap = listesceneplanifierbydate.stream().collect(Collectors.groupingBy(Calendrier::getDateexact));

        bookMap.forEach((k, v) -> System.out.println(k + " => " + v));

        String datedebutfinal = new SimpleDateFormat("dd MMMM yyyy").format(datedebutoriginal);
        String datefinfinal = new SimpleDateFormat("dd MMMM yyyy").format(datefinoriginal);

        mv.addObject("listescenetrue", listesceneplanifierbydate);
        mv.addObject("listedatedispo", bookMap);
        mv.addObject("datedebutplanning", datedebutfinal);
        mv.addObject("datefinplanning", datefinfinal);
        mv.setViewName("schedule-tab-1");
        return mv;
    }

    @RequestMapping(value = "/scene")
    public String getFilm(Model model) {
        model.addAttribute("film", dao.findAll(Film.class));
        return "film-list";
    }

    @RequestMapping(value = "/new-film")
    public String addFilm(Model model, @ModelAttribute Film film) {
        dao.create(film);
        model.addAttribute("film", dao.findAll(Film.class));
        return "film-list";
    }

    @RequestMapping(value = "/formulairescene")
    public String getFormScene(Model model, HttpServletRequest request, @ModelAttribute Scene scene, @ModelAttribute Film film) {
        List<Plateau> findAll = dao.findAll(Plateau.class);
       
        model.addAttribute("plateaus", findAll);
        
        
        HttpSession session = request.getSession(false);
        session.setAttribute("idfilm", film);
        return "FormulaireScene";
    }

    @RequestMapping(value = "action", method = RequestMethod.POST)
    public String getActionScene(Model model, @RequestParam("acteur[]") int[] idacteurs, HttpServletRequest request) {
        List<Acteur> listeacteurchoisi = new ArrayList<>();
        HttpSession session = request.getSession(false);
        Scene scene = (Scene) session.getAttribute("scene");
        ActeurScene acteurscene = new ActeurScene();
        for (int i = 0; i < idacteurs.length; i++) {
            acteurscene.setScene(scene.getIdscene());
            acteurscene.setActeur(idacteurs[i]);
            dao.create(acteurscene);
            if (idacteurs[i] != 0) {
                Acteur a = dao.findById(Acteur.class,
                        idacteurs[i]);
                System.out.println("Acteur choisi: " + a.getIdacteur() + " | " + a.getNomacteur() + " | " + a.getGenre());
                listeacteurchoisi.add(a);
            }
        }

        // MILA ALEFA ETO IHANY KOA ILA ACTEUR SCENE AO AMIN'I IOTY
        model.addAttribute("acteurscene", listeacteurchoisi);
        return "editscript";
    }

    @RequestMapping(value = "/getactionsacteur", method = RequestMethod.POST, params = "in-writing")
    public String getScriptActeurScene(@RequestParam("idscene") int idscene, @RequestParam(value = "idacteursceneactions[]", required = false) int[] idacteursceneactions, @RequestParam(value = "idactorsexist[]", required = false) int[] idactorsexist, @RequestParam(value = "scriptexist[]", required = false) String[] scriptsexist, @RequestParam(value = "dureeexist[]", required = false) String[] dureeexist, @RequestParam("idactors[]") int[] idactors, @RequestParam("script[]") String[] scripts, @RequestParam("duree[]") String[] durees, Model model) throws Exception {
        //public String getScriptActeurScene(@RequestParam("idscene") int idscene, @RequestParam("idacteursceneaction[]") int[] idacteursceneactions, @RequestParam('idactorsexist[]', required = false) int[] idactorsexist, Model model) throws Exception {

        // Get Scene
        Scene scene = dao.findById(Scene.class, idscene);

        // Update Existing Data
        if (idactorsexist != null) {
            for (int i = 0; i < idactorsexist.length; i++) {
                int idactorexists = idactorsexist[i];
                String scriptexists = scriptsexist[i];
                String dureeexists = dureeexist[i];
                int idacteursceneaction = idacteursceneactions[i];

                ActeurSceneAction acteurSceneAction = new ActeurSceneAction();
                acteurSceneAction.setIdacteursceneaction(idacteursceneaction);
                acteurSceneAction.setActeur(idactorexists);
                // set scene to action acteur
                acteurSceneAction.setScene(idscene);
                acteurSceneAction.setScript(scriptexists);
                acteurSceneAction.setDuree(dureeexists);
                dao.update(acteurSceneAction);
            }
        }

        // Get new data from form
        // Create new Data       
        if (scripts.length != 0) {
            for (int i = 0; i < scripts.length; i++) {
                int idactorsnew = idactors[i];
                String scriptsnew = scripts[i];
                String dureesnew = durees[i];

                ActeurSceneAction acteurSceneAction = new ActeurSceneAction();
                acteurSceneAction.setActeur(idactorsnew);
                // set scene to action acteur
                acteurSceneAction.setScene(idscene);
                acteurSceneAction.setScript(scriptsnew);
                acteurSceneAction.setDuree(dureesnew);
                dao.create(acteurSceneAction);
            }
        }

        // Set Status of Scene to In-writing (2)
        int statutenecriture = 2;
        scene.setStatusplanification(statutenecriture);
        dao.update(scene);

        List<Scene> listescenesfini = dao.findWhereStatusScene(Scene.class, statutenecriture);
        model.addAttribute("scenes", listescenesfini);

        List<Statusindispo> listefiltre = dao.findAll(Statusindispo.class);
        model.addAttribute("listefiltre", listefiltre);

        model.addAttribute("filtre", "2");

        return "listescene";
    }

    @RequestMapping(value = "/getactionsacteur", method = RequestMethod.POST, params = "finish")
    public String finishScene(@RequestParam("idscene") int idscene, @RequestParam("idacteursceneaction[]") int[] idacteursceneactions, @RequestParam("idactorsexist[]") int[] idactorsexist, @RequestParam("scriptexist[]") String[] scriptsexist, @RequestParam("dureeexist[]") String[] dureeexist, @RequestParam("idactors[]") int[] idactors, @RequestParam("script[]") String[] scripts, @RequestParam("duree[]") String[] durees, Model model) throws Exception {

        // Get Scene
        Scene scene = dao.findById(Scene.class, idscene);

        // Update Existing Data
        for (int i = 0; i < idactorsexist.length; i++) {
            int idactorexists = idactorsexist[i];
            String scriptexists = scriptsexist[i];
            String dureeexists = dureeexist[i];
            int idacteursceneaction = idacteursceneactions[i];

            ActeurSceneAction acteurSceneAction = new ActeurSceneAction();
            acteurSceneAction.setIdacteursceneaction(idacteursceneaction);
            acteurSceneAction.setActeur(idactorexists);
            // set scene to action acteur
            acteurSceneAction.setScene(idscene);
            acteurSceneAction.setScript(scriptexists);
            acteurSceneAction.setDuree(dureeexists);
            dao.update(acteurSceneAction);
        }

        // Get new data from form
        // Create new Data 
        if (scripts.length != 0) {
            for (int i = 0; i < idactors.length; i++) {
                int idactorsnew = idactors[i];
                String scriptsnew = scripts[i];
                String dureesnew = durees[i];

                ActeurSceneAction acteurSceneAction = new ActeurSceneAction();
                acteurSceneAction.setActeur(idactorsnew);
                // set scene to action acteur
                acteurSceneAction.setScene(idscene);
                acteurSceneAction.setScript(scriptsnew);
                acteurSceneAction.setDuree(dureesnew);
                dao.create(acteurSceneAction);
            }
        }

        // Set Status of Scene to Finish (3)
        int statutfini = 3;
        scene.setStatusplanification(statutfini);
        dao.update(scene);

        List<Scene> listescenesfini = dao.findWhereStatusScene(Scene.class, statutfini);
        model.addAttribute("scenes", listescenesfini);

        List<Statusindispo> listefiltre = dao.findAll(Statusindispo.class);
        model.addAttribute("listefiltre", listefiltre);

        model.addAttribute("filtre", "3");

        return "listescene";
    }

    @RequestMapping(value = "/scenes")
    public String getListeScene(Model model) {
        List<Scene> listescenes = dao.findWhereStatusScene(Scene.class, 3);
        model.addAttribute("scenes", listescenes);

        List<Statusindispo> listefiltre = dao.findAll(Statusindispo.class);
        model.addAttribute("listefiltre", listefiltre);

        model.addAttribute("filtre", "1");
        return "listescene";
    }

    @RequestMapping(value = "/filtre")
    public String getListeSceneFiltre(Model model, HttpServletRequest req) {
        // List<Scene> listescenes = dao.findAll(Scene.class);
        int status = Integer.parseInt(req.getParameter("filter"));
        List<Scene> listescenes = dao.findWhereStatusScene(Scene.class, status);
        model.addAttribute("scenes", listescenes);

        List<Statusindispo> listefiltre = dao.findAll(Statusindispo.class);
        model.addAttribute("listefiltre", listefiltre);

        model.addAttribute("filtre", status);
        return "listescene";
    }

    @RequestMapping(value = "/selectscene", method = RequestMethod.POST)
    public String getDateDebutFin(Model model, HttpServletRequest request) {
        Date datedabut = Date.valueOf(request.getParameter("datedebut"));
        Date datefin = Date.valueOf(request.getParameter("datefin"));

        // Create Session
        HttpSession session = request.getSession();

        // Add Scene to Session
        session.setAttribute("datedebutplanifie", datedabut);
        session.setAttribute("datefinplanifie", datefin);

        List<Scene> listescenes = dao.findWhereStatusScene(Scene.class, 3);
        model.addAttribute("sceneNoPlaned", listescenes);
        return "select-scene";
    }

    @RequestMapping(value = "/trayindisponible", method = RequestMethod.GET)
    public String trayUnavailable(Model model, HttpServletRequest request) {

        return "tray-list";
    }

    private List<Indisponibiliteplateau> checkDisponibilite(HttpServletRequest request) {

        // Create Session
        HttpSession session = request.getSession(true);

        // Get Date Debut, Date Fin Planning        
        Date datedebut = (Date) session.getAttribute("datedebutplanifie");
        Date datefin = (Date) session.getAttribute("datefinplanifie");
        List<Indisponibiliteplateau> newList = dao.getListIndispo(Indisponibiliteplateau.class, datedebut, datefin);
        System.out.println("-------------------" + newList.size());
        return newList;
    }

    @RequestMapping(value = "/scene/{idscene}", method = RequestMethod.GET)
    public String getScene(Model model, @PathVariable int idscene) {

        // Get Scene        
        Scene s = dao.findById(Scene.class, idscene);

        // Get Liste Acteurs Scene        
//        ActeurScene as = new ActeurScene();
//        as.setIdscene(idscene);
//        
//        List<ActeurScene> listeacteurscene = dao.findWhere(as);
        // Get Acteur Scene
        List<ActeurScene> listeacteursceneaction = dao.findwherescene(ActeurScene.class, idscene);

        // Add Acteur to Liste Acteur        
        List<Acteur> listeacteur = new ArrayList<>();

        // Get Script Acteur
        List<Vueacteurscene> listescriptacteurscene = dao.findwhereidscene(Vueacteurscene.class, idscene);

        // Get Liste Acteurs
        List<Acteur> listeacteurs = dao.findAll(Acteur.class);

        for (ActeurScene acteurscene : listeacteursceneaction) {
            System.out.println("Scene: " + acteurscene.getScene() + " |  Acteur: " + acteurscene.getActeur());
            Acteur a = dao.findById(Acteur.class, acteurscene.getActeur());
            listeacteur.add(a);
        }

        for (Vueacteurscene v : listescriptacteurscene) {
            System.out.println("Vue Script Acteur : Scene:" + v.getIdscene() + " | Acteur" + v.getNomacteur() + " " + v.getNomacteur() + " | Script: " + v.getScript() + " | Duree:" + v.getDuree());
        }

        List<Acteur> listeact = new ArrayList<>();

        // Check if Acteur is already en scene
        for (int i = 0; i < listeacteurs.size(); i++) {
            for (Acteur acteur : listeacteur) {
                if (!listeacteurs.get(i).equals(acteur)) {
                    //listeacteurs.remove(i);
                    listeact.add(listeacteurs.get(i));
                }
            }
        }

        for (Acteur acteur : listeact) {
            System.out.println("------> " + acteur.getNomacteur());
        }

        // Add Acteurs to View
        model.addAttribute("acteurs", listeacteurs);

        // Add Acteur to View
        model.addAttribute("acteurscene", listeacteur);

        // Add Script Acteur to View
        model.addAttribute("scriptsacteur", listescriptacteurscene);

        // Add Scene to View        
        model.addAttribute("scene", s);
        return "editscript";
    }

    @RequestMapping(value = "/scene/acteur", method = RequestMethod.POST)
    public String addActeurScene(Model model, @RequestParam("scene") int idscene, @RequestParam("acteur") int idacteur) {

        // Create new ActeurScene
        ActeurScene as = new ActeurScene();
        as.setScene(idscene);
        as.setActeur(idacteur);
        // Insert into Database        
        dao.create(as);

        // Get Scene        
        Scene s = dao.findById(Scene.class, idscene);

        // Get Acteur Scene
        List<ActeurScene> listeacteursceneaction = dao.findwherescene(ActeurScene.class, idscene);

        // Add Acteur to Liste Acteur        
        List<Acteur> listeacteur = new ArrayList<>();

        // Get Script Acteur
        List<Vueacteurscene> listescriptacteurscene = dao.findwhereidscene(Vueacteurscene.class, idscene);

        // Get Liste Acteurs
        List<Acteur> listeacteurs = dao.findAll(Acteur.class);

        for (ActeurScene acteurscene : listeacteursceneaction) {
            System.out.println("Scene: " + acteurscene.getScene() + " |  Acteur: " + acteurscene.getActeur());
            Acteur a = dao.findById(Acteur.class, acteurscene.getActeur());
            listeacteur.add(a);
        }

        for (Vueacteurscene v : listescriptacteurscene) {
            System.out.println("Vue Script Acteur : Scene:" + v.getIdscene() + " | Acteur" + v.getIdacteur() + " " + v.getNomacteur() + " | Script: " + v.getScript() + " | Duree:" + v.getDuree());
        }

        // Check if Acteur is already en scene
        for (int i = 0; i < listeacteurs.size(); i++) {
            for (Acteur acteur : listeacteur) {
                if (listeacteurs.get(i).equals(acteur)) {
                    listeacteurs.remove(i);
                }
            }
        }

        // Add Acteurs to View
        model.addAttribute("acteurs", listeacteurs);

        // Add Acteur to View
        model.addAttribute("acteurscene", listeacteur);

        // Add Script Acteur to View
        model.addAttribute("scriptsacteur", listescriptacteurscene);

        // Add Scene to View        
        model.addAttribute("scene", s);

        return "editscript";
    }

    @RequestMapping(value = "/planifierscene", method = RequestMethod.POST)
    public String planifierScene(@RequestParam("idscene[]") int[] idscenes, @RequestParam("datescene[]") String[] datescenes, @RequestParam("heuredebutscene[]") String[] heuredebutscenes, @RequestParam("heurefinscene[]") String[] heurefinscenes, Model model) {

        int statutsceneplanifiee = 4;

        // Get Data Scene by date        
        if (idscenes != null) {
            for (int i = 0; i < idscenes.length; i++) {
                int idscene = idscenes[i];
                String datescene = datescenes[i];
                String heuredebutscene = heuredebutscenes[i];
                String heurefinscene = heurefinscenes[i];

                if (heuredebutscene.toCharArray().length <= 5) {
                    heuredebutscene += ":00";
                }
                if (heurefinscene.toCharArray().length <= 5) {
                    heurefinscene += ":00";
                }

                // Create new Calendrier
                Calendrier c = new Calendrier();
                c.setDateexact(Date.valueOf(datescene));
                c.setHeuredebutexact(Time.valueOf(heuredebutscene));
                c.setHeurefinexact(Time.valueOf(heurefinscene));
                c.setScene(idscene);

                for (String heurefinscene1 : heurefinscenes) {
                    System.out.println(heurefinscene1);
                }

                // Insert new Calendrier
                dao.create(c);

                // Update Statut Scene
                // Get Scene by id
                Scene scene = dao.findById(Scene.class, idscene);

                // Update statut scene                
                scene.setStatusplanification(statutsceneplanifiee);
                dao.update(scene);
            }
        }

        return "schedule-tab-1";

    }

}
