package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;
import java.sql.Time;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Calendrier implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idcalendrier;
    private int scene;
    private Date dateexact;
    @DateTimeFormat(pattern = "hh:mm")
    private Time heuredebutexact;

    @DateTimeFormat(pattern = "hh:mm")
    private Time heurefinexact;

    public Time getHeuredebutexact() {
        return heuredebutexact;
    }

    public void setHeuredebutexact(Time heuredebutexact) {
        this.heuredebutexact = heuredebutexact;
    }

    public Time getHeurefinexact() {
        return heurefinexact;
    }

    public void setHeurefinexact(Time heurefinexact) {
        this.heurefinexact = heurefinexact;
    }

    public int getIdcalendrier() {
        return idcalendrier;
    }

    public void setIdcalendrier(int idcalendrier) {
        this.idcalendrier = idcalendrier;
    }

    public int getScene() {
        return scene;
    }

    public void setScene(int scene) {
        this.scene = scene;
    }

    public Date getDateexact() {
        return dateexact;
    }

    public void setDateexact(Date dateexact) {
        this.dateexact = dateexact;
    }
}
