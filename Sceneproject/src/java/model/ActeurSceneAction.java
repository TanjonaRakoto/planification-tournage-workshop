package model;
import java.io.Serializable;
import java.sql.Time;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalTime;

@Entity
public class ActeurSceneAction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idacteursceneaction;
    private int acteur;
    private int scene;
    private String script;
    @DateTimeFormat(pattern = "hh:mm")
    private Time duree;

    public int getIdacteursceneaction() {
        return idacteursceneaction;
    }

    public void setIdacteursceneaction(int idacteursceneaction) {
        this.idacteursceneaction = idacteursceneaction;
    }

    public int getActeur() {
        return acteur;
    }

    public void setActeur(int acteur) {
        this.acteur = acteur;
    }

    public int getScene() {
        return scene;
    }

    public void setScene(int scene) {
        this.scene = scene;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        LocalTime time = LocalTime.parse(duree);
        Time timevao = Time.valueOf(time) ;
        this.duree = timevao;
    }
}
