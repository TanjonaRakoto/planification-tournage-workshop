/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author P14A-GOLD
 */
@Entity
public class Vueacteurscene implements Serializable {
 @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idacteursceneaction;

    public int getIdacteursceneaction() {
        return idacteursceneaction;
    }

    public void setIdacteursceneaction(int idacteursceneaction) {
        this.idacteursceneaction = idacteursceneaction;
    }

    public String getNomacteur() {
        return nomacteur;
    }

    public void setNomacteur(String nomacteur) {
        this.nomacteur = nomacteur;
    }

    public String getPhotoacteur() {
        return photoacteur;
    }

    public void setPhotoacteur(String photoacteur) {
        this.photoacteur = photoacteur;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }

    public int getIdscene() {
        return idscene;
    }

    public void setIdscene(int idscene) {
        this.idscene = idscene;
    }

    public String getNomscene() {
        return nomscene;
    }

    public void setNomscene(String nomscene) {
        this.nomscene = nomscene;
    }

    public String getNomplateau() {
        return nomplateau;
    }

    public void setNomplateau(String nomplateau) {
        this.nomplateau = nomplateau;
    }
    
    
    public Date getDatescene() {
        return datescene;
    }

    public void setDatescene(Date datescene) {
        this.datescene = datescene;
    }

    public int getIdacteur() {
        return idacteur;
    }

    public void setIdacteur(int idacteur) {
        this.idacteur = idacteur;
    }
    
    
    
    int idacteur;
    String nomacteur;
    String photoacteur;
    String script;
    Time duree;
    int idscene;
    String nomscene;
    String nomplateau;
    Date datescene;
}
