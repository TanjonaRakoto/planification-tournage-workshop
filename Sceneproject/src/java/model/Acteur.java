package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Acteur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idacteur;
    private String nomacteur;
    private String genre;
    private String photoacteur;

    public String getPhotoacteur() {
        return photoacteur;
    }

    public void setPhotoacteur(String photoacteur) {
        this.photoacteur = photoacteur;
    }

    public int getIdacteur() {
        return idacteur;
    }

    public void setIdacteur(int idacteur) {
        this.idacteur = idacteur;
    }

    public String getNomacteur() {
        return nomacteur;
    }

    public void setNomacteur(String nomacteur) {
        this.nomacteur = nomacteur;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
