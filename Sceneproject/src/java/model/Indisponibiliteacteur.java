/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Meylis
 */
@Entity
public class Indisponibiliteacteur  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idindispoacteur;
    @JoinColumn(name = "acteurindispo", referencedColumnName = "idacteur")
    @ManyToOne(optional = false)
    Acteur acteurindispo;
    Date dateindispo;
    String motifindispo;

    public int getIdindispoacteur() {
        return idindispoacteur;
    }

    public void setIdindispoacteur(int idindispoacteur) {
        this.idindispoacteur = idindispoacteur;
    }

    public Acteur getActeurindispo() {
        return acteurindispo;
    }

    public void setActeurindispo(Acteur acteurindispo) {
        this.acteurindispo = acteurindispo;
    }

    public Date getDateindispo() {
        return dateindispo;
    }

    public void setDateindispo(Date dateindispo) {
        this.dateindispo = dateindispo;
    }

    public String getMotifindispo() {
        return motifindispo;
    }

    public void setMotifindispo(String motifindispo) {
        this.motifindispo = motifindispo;
    }
        
}
