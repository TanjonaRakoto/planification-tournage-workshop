/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Tanjoniaina
 */
@Entity
public class Statusindispo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idstatus;

    public Integer getIdstatus() {
        return idstatus;
    }

    public void setIdstatus(int idstatus) {
        this.idstatus = idstatus;
    }
    private String valeurstatus;

    public String getValeurstatus() {
        return valeurstatus;
    }

    public void setValeurstatus(String valeurstatus) {
        this.valeurstatus = valeurstatus;
    }
}
