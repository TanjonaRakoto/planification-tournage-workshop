/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Meylis
 */
@Entity
public class Film implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idfilm;    
    private String nomfilm;
    private String nomrealisateur;
    private String descriptionfilm;
    private String imagefilm;

    public int getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(int idfilm) {
        this.idfilm = idfilm;
    }

    public String getNomfilm() {
        return nomfilm;
    }

    public void setNomfilm(String nomfilm) {
        this.nomfilm = nomfilm;
    }

    public String getNomrealisateur() {
        return nomrealisateur;
    }

    public void setNomrealisateur(String nomrealisateur) {
        this.nomrealisateur = nomrealisateur;
    }

    public String getDescriptionfilm() {
        return descriptionfilm;
    }

    public void setDescriptionfilm(String descriptionfilm) {
        this.descriptionfilm = descriptionfilm;
    }

    public String getImagefilm() {
        return imagefilm;
    }

    public void setImagefilm(String imagefilm) {
        this.imagefilm = imagefilm;
    }
    
    
}
