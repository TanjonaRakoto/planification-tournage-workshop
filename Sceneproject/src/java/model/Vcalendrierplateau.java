/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Meylis
 */
@Entity
@Table(name = "v_calendrier_plateau")
public class Vcalendrierplateau implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idcalendrier;
    private Date dateexact;
    private Time heuredebutexact;
    
    @JoinColumn(name = "scene", referencedColumnName = "idscene")
    @ManyToOne(optional = false)
    private Scene scene;    

    
    private int idplateau;

    public int getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(int idplateau) {
        this.idplateau = idplateau;
    }        
    
    public Time getHeuredebutexact() {
        return heuredebutexact;
    }

    public void setHeuredebutexact(Time heuredebutexact) {
        this.heuredebutexact = heuredebutexact;
    }

    public Time getHeurefinexact() {
        return heurefinexact;
    }

    public void setHeurefinexact(Time heurefinexact) {
        this.heurefinexact = heurefinexact;
    }
     private Time heurefinexact;
    
    public int getIdcalendrier() {
        return idcalendrier;
    }

    public void setIdcalendrier(int idcalendrier) {
        this.idcalendrier = idcalendrier;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Date getDateexact() {
        return dateexact;
    }

    public void setDateexact(Date dateexact) {
        this.dateexact = dateexact;
    }
}
