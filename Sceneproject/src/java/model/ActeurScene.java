/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author goldandrianavalona
 */
@Entity
@Table(name = "acteurscene")
public class ActeurScene implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idacteurscene;
    private int acteur;
    private int scene;

    public int getScene() {
        return scene;
    }

    public void setScene(int scene) {
        this.scene = scene;
    }

    public int getActeur() {
        return acteur;
    }

    public void setActeur(int acteur) {
        this.acteur = acteur;
    }

    public ActeurScene() {
    }

    public ActeurScene(Integer idacteurscene) {
        this.idacteurscene = idacteurscene;
    }

    public Integer getIdacteurscene() {
        return idacteurscene;
    }

    public void setIdacteurscene(Integer idacteurscene) {
        this.idacteurscene = idacteurscene;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idacteurscene != null ? idacteurscene.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActeurScene)) {
            return false;
        }
        ActeurScene other = (ActeurScene) object;
        if ((this.idacteurscene == null && other.idacteurscene != null) || (this.idacteurscene != null && !this.idacteurscene.equals(other.idacteurscene))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Acteurscene[ idacteurscene=" + idacteurscene + " ]";
    }

}
