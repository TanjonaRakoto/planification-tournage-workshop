/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Meylis
 */
@Entity
public class Indisponibiliteplateau  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idindispo;
    @JoinColumn(name = "plateauindispo", referencedColumnName = "idplateau")
    @ManyToOne(optional = false)
    Plateau plateauindispo;
    Date dateindispo;
    String motifindispo;

    public int getIdindispo() {
        return idindispo;
    }

    public void setIdindispo(int idindispo) {
        this.idindispo = idindispo;
    }

    public Plateau getPlateauindispo() {
        return plateauindispo;
    }

    public void setPlateauindispo(Plateau plateauindispo) {
        this.plateauindispo = plateauindispo;
    }

    public Date getDateindispo() {
        return dateindispo;
    }

    public void setDateindispo(Date dateindispo) {
        this.dateindispo = dateindispo;
    }

    public String getMotifindispo() {
        return motifindispo;
    }

    public void setMotifindispo(String motifindispo) {
        this.motifindispo = motifindispo;
    }
    
    
    
}
