/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author P14A-GOLD
 */
@Entity
public class Listesceneordertrue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idcalendrier;
    
    @JoinColumn(name = "scene", referencedColumnName = "idscene")
    @ManyToOne(optional = false)
    Scene scene;
    
    @JoinColumn(name = "plateau", referencedColumnName = "idplateau")
    @ManyToOne(optional = false)
    Plateau plateau;        
    
    Date dateexact;
    Time heuredebutexact;
    Time heurefinexact;
    String nomscene;

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    
    
    public String getNomscene() {
        return nomscene;
    }

    public void setNomscene(String nomscene) {
        this.nomscene = nomscene;
    }

    public int getIdcalendrier() {
        return idcalendrier;
    }

    public void setIdcalendrier(int idcalendrier) {
        this.idcalendrier = idcalendrier;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Date getDateexact() {
        return dateexact;
    }

    public void setDateexact(Date dateexact) {
        this.dateexact = dateexact;
    }

    public Time getHeuredebutexact() {
        return heuredebutexact;
    }

    public void setHeuredebutexact(Time heuredebutexact) {
        this.heuredebutexact = heuredebutexact;
    }

    public Time getHeurefinexact() {
        return heurefinexact;
    }

    public void setHeurefinexact(Time heurefinexact) {
        this.heurefinexact = heurefinexact;
    }

}
