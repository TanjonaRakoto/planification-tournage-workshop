package model;

import java.io.Serializable;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalTime;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Scene implements Serializable {

    @Id
    @GeneratedValue(generator = "acteur_idacteur_seq")
    @GenericGenerator(name = "acteur_idacteur_seq", strategy = "increment")
    private int idscene;
    private Date datescene;
    @DateTimeFormat(pattern = "hh:mm")
    private Time heuredebut;
    @DateTimeFormat(pattern = "hh:mm")
    private Time heurefin;

    @JoinColumn(name = "plateau", referencedColumnName = "idplateau")
    @ManyToOne(optional = false)
    private Plateau plateau;

    @JoinColumn(name = "film", referencedColumnName = "idfilm")
    @ManyToOne(optional = false)
    private Film film;

    @JoinColumn(name = "auteur", referencedColumnName = "idauteur")
    @ManyToOne(optional = false)
    private Auteur auteur;

    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }    
    
    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    private String nomscene;
    private int statusplanification;

    public int getStatusplanification() {
        return statusplanification;
    }

    public void setStatusplanification(int statusplanification) {
        this.statusplanification = statusplanification;
    }

    public String getNomscene() {
        return nomscene;
    }

    public void setNomscene(String nomscene) {
        this.nomscene = nomscene;
    }

    public int getIdscene() {
        return idscene;
    }

    public void setIdscene(int idscene) {
        this.idscene = idscene;
    }

    public Date getDatescene() {
        return datescene;
    }

    public void setDatescene(String datescene) {
        Date date = Date.valueOf(datescene);
        this.datescene = date;
    }

    public Time getHeuredebut() {
        return heuredebut;
    }

    public void setHeuredebut(String heuredebut) {

        LocalTime time = LocalTime.parse(heuredebut);
        Time timevao = Time.valueOf(time);
        this.heuredebut = timevao;
    }

    public Time getHeurefin() {
        return heurefin;
    }

    public void setHeurefin(String heurefin) {
        LocalTime time = LocalTime.parse(heurefin);
        Time timevao = Time.valueOf(time);
        this.heurefin = timevao;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }
}
