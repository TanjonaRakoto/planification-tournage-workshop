/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author P14A-GOLD
 */
@Entity
public class Listetrue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idcalendrier;
    int scene;
    Date dateexact;
    Time heuredebutexact;
    Time heurefinexact;

    public int getIdcalendrier() {
        return idcalendrier;
    }

    public void setIdcalendrier(int idcalendrier) {
        this.idcalendrier = idcalendrier;
    }

    public int getScene() {
        return scene;
    }

    public void setScene(int scene) {
        this.scene = scene;
    }

    public Date getDateexact() {
        return dateexact;
    }

    public void setDateexact(Date dateexact) {
        this.dateexact = dateexact;
    }

    public Time getHeuredebutexact() {
        return heuredebutexact;
    }

    public void setHeuredebutexact(Time heuredebutexact) {
        this.heuredebutexact = heuredebutexact;
    }

    public Time getHeurefinexact() {
        return heurefinexact;
    }

    public void setHeurefinexact(Time heurefinexact) {
        this.heurefinexact = heurefinexact;
    }

}
