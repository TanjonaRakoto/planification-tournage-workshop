/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Fanjava
 */
@Entity
public class Parametrage implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    Date Datetournage;
    Time timedebut;
    Time timefin;
    Time timedebutexact;
    Time timefinexact;

    public int getId() {
        return id;
    }

    public Date getDatetournage() {
        return Datetournage;
    }

    public void setDatetournage(Date Datetournage) {
        this.Datetournage = Datetournage;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Time getTimedebut() {
        return timedebut;
    }

    public void setTimedebut(Time timedebut) {
        this.timedebut = timedebut;
    }

    public Time getTimefin() {
        return timefin;
    }

    public void setTimefin(Time timefin) {
        this.timefin = timefin;
    }

    public Time getTimedebutexact() {
        return timedebutexact;
    }

    public void setTimedebutexact(Time timedebutexact) {
        this.timedebutexact = timedebutexact;
    }

    public Time getTimefinexact() {
        return timefinexact;
    }

    public void setTimefinexact(Time timefinexact) {
        this.timefinexact = timefinexact;
    }
    
    
    
}
