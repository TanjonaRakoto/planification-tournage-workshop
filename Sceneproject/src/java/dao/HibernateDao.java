package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;

import java.io.Serializable;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javassist.compiler.TokenId;
import model.Calendrier;
import model.Indisponibiliteplateau;
import model.Parametrage;
import model.Scene;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;

import org.hibernate.criterion.Restrictions;

//Hibernate 3.0
public class HibernateDao {

    private SessionFactory sessionFactory;

    public <T> T create(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(entity);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> T findById(Class<T> clazz, Serializable id) {
        Session session = sessionFactory.openSession();
        T entity = (T) session.get(clazz, id);
        session.close();
        return entity;
    }

    public <T> List<T> findorderby(Class<T> tClass) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).addOrder(Order.asc("datescene")).list();
        session.close();
        return results;
    }

    public <T> List<T> findgroupby(Class<T> tClass, String column, String column1) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).setProjection(Projections.projectionList().add(Projections.groupProperty(column)).add(Projections.groupProperty(column1))).list();
        session.close();
        return results;
    }

    public <T> List<T> findAll(Class<T> tClass) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).list();
        session.close();
        return results;
    }

    public <T> List<T> findwhereidscene(Class<T> tClass, int idscene) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).
                add(Restrictions.eq("idscene", idscene)).list();
        session.close();
        return results;
    }

    public <T> List<T> findwhereidplateau(Class<T> tClass, int idplateau) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).
                add(Restrictions.ne("idplateau", idplateau)).list();
        session.close();
        return results;
    }

    public <T> List<T> findWhereStatusScene(Class<T> tClass, int status) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).
                add(Restrictions.eq("statusplanification", status)).list();
        session.close();
        return results;
    }

    public <T> List<T> findwheredateindispo(Class<T> tClass, Date dateindispo) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).
                add(Restrictions.eq("dateindispo", dateindispo)).list();
        session.close();
        return results;
    }

    public void insertionsceneExact() throws ParseException {
        List<Scene> listescene = findorderby(Scene.class);
        Calendrier calendrier = new Calendrier();
        for (int i = 0; i < listescene.size(); i++) {
            if (i < listescene.size() - 1) {

                if (listescene.get(i).getHeuredebut().equals(listescene.get(i + 1).getHeuredebut()) && listescene.get(i).getDatescene().equals(listescene.get(i + 1).getDatescene())) {
                    ///////get intervale entre 2 time
                    int intervale = getaddhours(listescene.get(i + 1).getHeuredebut(), listescene.get(i + 1).getHeurefin());
                    ////get heure avec addition intervale
                    Time heurefin = getFinexact(listescene.get(i).getHeurefin().toString(), intervale);
                    //////
                    calendrier.setIdcalendrier(listescene.get(i).getIdscene());
                    calendrier.setDateexact(listescene.get(i).getDatescene());
                    calendrier.setHeuredebutexact(listescene.get(i).getHeurefin());
                    calendrier.setHeurefinexact(heurefin);
                    calendrier.setScene(listescene.get(i).getIdscene());
                    create(calendrier);

                } else if (listescene.get(i).getHeuredebut() != (listescene.get(i + 1).getHeuredebut()) && listescene.get(i).getDatescene() != (listescene.get(i + 1).getDatescene())) {
                    calendrier.setIdcalendrier(listescene.get(i).getIdscene());
                    calendrier.setDateexact(listescene.get(i).getDatescene());
                    calendrier.setHeuredebutexact(listescene.get(i).getHeuredebut());
                    calendrier.setHeurefinexact(listescene.get(i).getHeurefin());
                    calendrier.setScene(listescene.get(i).getIdscene());

                    create(calendrier);
                }
            } else {
                calendrier.setIdcalendrier(listescene.get(i).getIdscene());
                calendrier.setDateexact(listescene.get(i).getDatescene());
                calendrier.setHeuredebutexact(listescene.get(i).getHeuredebut());
                calendrier.setHeurefinexact(listescene.get(i).getHeurefin());
                calendrier.setScene(listescene.get(i).getIdscene());

                create(calendrier);
                break;
            }
        }

    }

    public Time getFinexact(String Timefin, int addhours) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date datefin = format.parse(Timefin);
        Calendar cal = Calendar.getInstance();
        cal.setTime(datefin);
        cal.add(Calendar.HOUR, addhours); // this will add two hours
        datefin = cal.getTime();
        Time heurreFinExact = Time.valueOf(datefin.getHours() + ":" + datefin.getMinutes() + ":" + datefin.getSeconds());
        return heurreFinExact;
    }

    public int getaddhours(Time debut, Time fin) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date date1 = format.parse(fin.toString());
        Date date2 = format.parse(debut.toString());
        int difference = date1.getHours() - date2.getHours();
        return difference;
    }

    public Time transformTime(LocalTime localtime) throws ParseException {
        Time temps = Time.valueOf(localtime);
        return temps;
    }

    public static void main(String[] args) {

//        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
//        Date datefin = format.parse("08:00:00");
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(datefin);
//        cal.add(Calendar.HOUR, 2); // this will add two hours
//        datefin = cal.getTime();
//        Time heurreExact = Time.valueOf(datefin.getHours() + ":" + datefin.getMinutes() + ":" + datefin.getSeconds());
//
//        Date date1 = format.parse("08:00:00");
//        Date date2 = format.parse("10:00:00");
//        int difference = date2.getHours() - date1.getHours();
//
    }

    public <T> List<T> findWhere(T entity) {
        Session session = sessionFactory.openSession();
        Example example = Example.create(entity).ignoreCase();
        List<T> results = session.createCriteria(entity.getClass()).add(example).list();
        session.close();
        return results;
    }

    public <T> List<T> paginateWhere(T entity, int offset, int size) {
        Session session = sessionFactory.openSession();
        Example example = Example.create(entity).ignoreCase();
        List<T> results = session.createCriteria(entity.getClass())
                .add(example)
                .setFirstResult(offset)
                .setMaxResults(offset + size).list();
        session.close();
//        ArrayList<E>
        return results;
    }

    public <T> List<T> paginate(Class<T> clazz, int offset, int size) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(clazz)
                .setFirstResult(offset)
                .setMaxResults(offset + size).list();
        session.close();
        return results;
    }

    public <T> List<T> paginate(Class<T> clazz, int offset, int size, String orderBy, boolean orderAsc) {
        Session session = sessionFactory.openSession();
        Order order = (orderAsc) ? Order.asc(orderBy) : Order.desc(orderBy);
        List<T> results = session.createCriteria(clazz)
                .addOrder(order)
                .setFirstResult(offset)
                .setMaxResults(offset + size).list();
        session.close();
        return results;
    }

    public void deleteById(Class tClass, Serializable id) {
        delete(findById(tClass, id));
    }

    public void delete(Object entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(entity);
        transaction.commit();
        session.close();
    }

    public <T> T update(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> List<T> findwherescene(Class<T> tClass, int scene) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).
                add(Restrictions.eq("scene", scene)).list();
        session.close();
        return results;
    }

    public <T> List<T> getListIndispo(Class<T> tClass, Date datedebut, Date datefin) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(tClass);
        criteria.add(Restrictions.ge("dateindispo", datedebut));
        criteria.add(Restrictions.lt("dateindispo", datefin));
        List<T> results = criteria.list();
        session.close();
        return results;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
