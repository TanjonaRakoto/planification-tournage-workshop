<%@ page import="java.util.List" %>
<%@ page import="model.Acteur" %><%--
  Created by IntelliJ IDEA.
  User: aina
  Date: 2023-01-11
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<%
    List<Acteur> acteurs = (List<Acteur>) request.getAttribute("acteurscene");
%>

<script type="text/javascript">
    function sendData(data){
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function (){
    if (xhr.readyState != 4 || xhr.status != 200){
    if (xhr.responseText.length != 0){
    xhr.abort();
    }
    }
    }

    xhr.addEventListener("error",function(event){
    alert("Quelque Chose s'est mal passe");
    });

    xhr.open("POST","http://localhost:8081/film/scene/getactionsacteur");
    xhr.setRequestHeader("Content-Type","application/json;charset=UTF-8");
    xhr.send(data);
    }

    // Get Data from Form
    function addActionActeur(){
        let idacteur = document.getElementById("acteur").value;
        let scriptacteur = document.getElementById("script").value;
        let time = document.getElementById("time").value;
    }

    // Send Data to Web Service

</script>

<form action="${pageContext.request.contextPath}/film/scene/getactionsacteur" method="post">
    <table>
        <th>Acteur</th>
        <th>Script</th>
        <th>Duree</th>
        <th></th>
        <tr>
            <td>
                <select id="acteur" name="acteur">
                    <% for (Acteur a : acteurs) {%>
                    <option value="<%= a.getIdacteur()%>"><%= a.getNomacteur()%></option>
                    <% } %>
                </select>
            </td>
            <td>
                <textarea id="script" name="script"></textarea>
            </td>
            <td>
                <input id="time" type="time" name="duree">
            </td>
            <td>
                <button onclick="addActionActeur()">Ajouter</button>
            </td>
        </tr>
    </table>
    <input type="submit" value="Valider">
</form>

</body>
</html>
