<%@ page import="model.*" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <!-- speakers-117:31-->

    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Edit Script Scene</title>

        <!-- CSS
      ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <link rel="stylesheet" href="fonts/font-awesome.min.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">
        <script src="${pageContext.request.contextPath}/ressources/js/jquery-3.6.0.min.js"></script>


    </head>

    <% List<Acteur> acteurs = (List<Acteur>) request.getAttribute("acteurscene");
        Scene scene = (Scene) request.getAttribute("scene");
        List<Vueacteurscene> listeactionacteur = (List<Vueacteurscene>) request.getAttribute("scriptsacteur");
        List<Acteur> listeacteurs = ((List<Acteur>) request.getAttribute("acteurs"));
    %>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png"
                                 alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a
                                        scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i
                                            class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes"
                                               class="">Scene List</a></li>
                                        <li><a
                                                href="${pageContext.request.contextPath}/trayindisponible">Tray
                                                Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area"
                 style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2>Scripts</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Exibit /</a>
                            </li>
                            <li>
                                Edit script for scene
                            </li>
                        </ol>
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->



            <!-- ts speaker start-->
            <section id="ts-speakers" class="ts-speakers speaker-classic"
                     style="background-image:url(${pageContext.request.contextPath}/ressources/images/speakers/speaker_bg.png)">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <h2 class="section-title text-center">
                                <span>Let's Go</span>
                                Action/Scripts
                            </h2>
                        </div><!-- col end-->
                    </div><!-- row end-->
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <!-- Scene duration : <strong>
                                    <%= scene.getHeuredebut()%>
                                </strong> to <strong>
                                    <%= scene.getHeurefin()%>
                                </strong> -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <button id="bnt-modify" role="button"
                                                    class="btn-link btn btn-sm" data-toggle="modal"
                                                    data-target="#newActor"
                                                    style="background-color: #ff9c0d; color: white;">Add new Actor <i
                                                    class="icon icon-plus"></i></button>
                                        </div>
                                        <div class="col">
                                            <div class="btn-group float-right mt-2">
                                                <button id="plus"
                                                        style="border:none; background-color: #f7f7f7; float: right; cursor: pointer;">
                                                    <i class="fa fa-plus-circle"
                                                       style="float: right;font-size: 25px;color: green;"></i>
                                                </button>
                                                <button id="moins"
                                                        style="border:none; background-color: #f7f7f7; float: right; cursor: pointer; visibility: hidden;">
                                                    <i class="fa fa-minus-circle"
                                                       style="float: right;font-size: 25px;color: red;"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form
                                    action="${pageContext.request.contextPath}/getactionsacteur"
                                    method="post">
                                    <input type="hidden" name="idscene"
                                           value="<%= scene.getIdscene()%>" />
                                    <div class="card-body" id="card-body">
                                        <% for (Vueacteurscene a : listeactionacteur) {
                                        %>
                                        <input type="hidden"
                                               name="idacteursceneactions[]"
                                               value="<%= a.getIdacteursceneaction()%>" />
                                        <div id="row-script-exist">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="actors"
                                                           class="text-info text-center">Actor</label>
                                                    <select class="form-control"
                                                            id="actors"
                                                            name="idactorsexist[]">
                                                        <option
                                                            value="<%= a.getIdacteur()%>">
                                                            <%= a.getNomacteur()%>
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-8">
                                                    <label for="script"
                                                           class="text-info text-center">Script</label>
                                                    <textarea type=""
                                                              class="form-control"
                                                              name="scriptexist[]"
                                                              style="width: 100%;"
                                                              rows="2"
                                                              id="script"><%= a.getScript()%></textarea>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="duration"
                                                           class="text-info">Duration</label>
                                                    <input type="time"
                                                           class="form-control" min="0"
                                                           name="dureeexist[]"
                                                           value="<%= a.getDuree()%>"
                                                           id="duration" />
                                                </div>
                                            </div>
                                        </div>
                                        <% } %>
                                    </div>

                                    <hr>

                                    <h3 class="card-text">Ajouter Script</h3>
                                    <div class="card-body" id="card-body-not-exists">
                                        <div id="row-script">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="actors"
                                                           class="text-info text-center">Actor</label>
                                                    <select class="form-control"
                                                            id="actors" name="idactors[]">
                                                        <% for (Acteur a : acteurs) {%>
                                                        <option
                                                            value="<%= a.getIdacteur()%>">
                                                            <%= a.getNomacteur()%>
                                                        </option>
                                                        <% }%>
                                                    </select>
                                                </div>
                                                <div class="col-md-8">
                                                    <label for="script"
                                                           class="text-info text-center">Script</label>
                                                    <textarea type=""
                                                              class="form-control"
                                                              name="script[]"
                                                              style="width: 100%;" rows="2"
                                                              id="script"></textarea>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="duration"
                                                           class="text-info">Duration</label>
                                                    <input type="time"
                                                           class="form-control" min="0"
                                                           name="duree[]" id="duration" />
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="btn-group float-right">
                                            <input class="btn btn-sm float-right"
                                                   type="submit" name="in-writing"
                                                   value="In-Writing"
                                                   style="background-color: #ff9c0d;" />
                                            <input class="btn btn-sm ml-1" type="submit"
                                                   name="finish" value="Finish" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- row end-->
                </div><!-- container end-->

                <!-- shap img-->
                <div class="speaker-shap">
                    <img class="shap1"
                         src="${pageContext.request.contextPath}/ressources/images/shap/home_speaker_memphis1.png"
                         alt="">
                    <img class="shap2"
                         src="${pageContext.request.contextPath}/ressources/images/shap/home_speaker_memphis2.png"
                         alt="">
                    <img class="shap3"
                         src="${pageContext.request.contextPath}/ressources/images/shap/home_speaker_memphis3.png"
                         alt="">
                </div>
                <!-- shap img end-->
            </section>

            <div id="newActor" class="modal fade" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>New Actors</h4><button class="btn-close" type="button"
                                                       data-dismiss="modal" aria-label="Close"
                                                       data-target="#newActor"></button>
                        </div>
                        <div class="modal-body">
                            <form
                                action="${pageContext.request.contextPath}/scene/acteur"
                                method="post">
                                <input type="hidden" name="scene"
                                       value="<%= scene.getIdscene()%>" />
                                <div class="form-group mb-3">
                                    <label for="select">Actors</label>
                                    <div class="input-group">
                                        <!-- <input class="form-control" type="date" required placeholder="Start date" /> -->
                                        <select class="form-control" id="select"
                                                name="acteur">
                                            <% for (Acteur a : listeacteurs) {%>
                                            <option value="<%= a.getIdacteur()%>">
                                                <%= a.getNomacteur()%>
                                            </option>
                                            <% }%>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <button class="btn btn-sm" style="width: 100%;"
                                            type="submit">Enter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
            <script
            src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
            <script
            src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>

            <script>
                var i = 0; // pour l'incrementation du compteur
                $(document).ready(function () {
                    var row = ($("#row-script").html());
                    // Enlever une ligen
                    $("#moins").click(function () {
                        $("#row-script .row:last").remove();
                        $("#row-script hr:last").remove();
                        if (i != 0) { // si i n'est pas encore égale à zero
                            i--; // On décrémente la valeur de i
                        }

                        if (i == 0) {
                            $("#moins").css("visibility", "hidden");
                        }

                    });



                    // Ajouter une autre ligne d'ecriture
                    $("#plus").click(function () {
                        i++; // incremente la valeur de i
                        $("#row-script").append(row);

                        if (i > 0) {
                            $("#moins").css("visibility", "visible");
                        }

                    });

                });
            </script>

        </div>
        <!-- Body inner end -->
    </body>


    <!-- speakers-117:31-->

</html>