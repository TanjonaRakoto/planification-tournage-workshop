--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

-- Started on 2023-03-21 17:35:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3460 (class 1262 OID 16394)
-- Name: calendrier; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE calendrier WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'French_France.1252';


ALTER DATABASE calendrier OWNER TO postgres;

\connect calendrier

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16395)
-- Name: acteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.acteur (
    idacteur integer NOT NULL,
    nomacteur character varying(50) NOT NULL,
    genre character varying(7) NOT NULL,
    photoacteur character varying(50) NOT NULL
);


ALTER TABLE public.acteur OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16398)
-- Name: acteur_idacteur_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.acteur_idacteur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.acteur_idacteur_seq OWNER TO postgres;

--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 210
-- Name: acteur_idacteur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.acteur_idacteur_seq OWNED BY public.acteur.idacteur;


--
-- TOC entry 233 (class 1259 OID 16547)
-- Name: seq_acteurscene; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seq_acteurscene
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_acteurscene OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16399)
-- Name: acteurscene; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.acteurscene (
    acteur integer,
    scene integer,
    idacteurscene integer DEFAULT nextval('public.seq_acteurscene'::regclass) NOT NULL
);


ALTER TABLE public.acteurscene OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16402)
-- Name: acteursceneaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.acteursceneaction (
    idacteursceneaction integer NOT NULL,
    acteur integer,
    scene integer NOT NULL,
    script text NOT NULL,
    duree time(6) without time zone NOT NULL
);


ALTER TABLE public.acteursceneaction OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16407)
-- Name: acteursceneaction_idacteursceneaction_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.acteursceneaction_idacteursceneaction_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.acteursceneaction_idacteursceneaction_seq OWNER TO postgres;

--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 213
-- Name: acteursceneaction_idacteursceneaction_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.acteursceneaction_idacteursceneaction_seq OWNED BY public.acteursceneaction.idacteursceneaction;


--
-- TOC entry 237 (class 1259 OID 32804)
-- Name: auteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auteur (
    idauteur integer NOT NULL,
    nomauteur character varying
);


ALTER TABLE public.auteur OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 32803)
-- Name: auteur_idauteur_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auteur_idauteur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auteur_idauteur_seq OWNER TO postgres;

--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 236
-- Name: auteur_idauteur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auteur_idauteur_seq OWNED BY public.auteur.idauteur;


--
-- TOC entry 214 (class 1259 OID 16408)
-- Name: calendrier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calendrier (
    idcalendrier integer NOT NULL,
    scene integer NOT NULL,
    dateexact date NOT NULL,
    heuredebutexact time without time zone,
    heurefinexact time without time zone
);


ALTER TABLE public.calendrier OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16411)
-- Name: calendrier_idcalendrier_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calendrier_idcalendrier_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calendrier_idcalendrier_seq OWNER TO postgres;

--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 215
-- Name: calendrier_idcalendrier_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calendrier_idcalendrier_seq OWNED BY public.calendrier.idcalendrier;


--
-- TOC entry 216 (class 1259 OID 16412)
-- Name: film; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.film (
    idfilm integer NOT NULL,
    nomfilm character varying NOT NULL,
    nomrealisateur character varying NOT NULL,
    descriptionfilm character varying,
    imagefilm character varying
);


ALTER TABLE public.film OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16417)
-- Name: film_idfilm_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.film_idfilm_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.film_idfilm_seq OWNER TO postgres;

--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 217
-- Name: film_idfilm_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.film_idfilm_seq OWNED BY public.film.idfilm;


--
-- TOC entry 234 (class 1259 OID 32789)
-- Name: indisponibiliteacteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.indisponibiliteacteur (
    idindispoacteur integer NOT NULL,
    acteurindispo integer,
    dateindispo date,
    motifindispo text DEFAULT 'en tournage'::text
);


ALTER TABLE public.indisponibiliteacteur OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 32795)
-- Name: indisponibiliteacteur_idindispoacteur_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.indisponibiliteacteur_idindispoacteur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indisponibiliteacteur_idindispoacteur_seq OWNER TO postgres;

--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 235
-- Name: indisponibiliteacteur_idindispoacteur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.indisponibiliteacteur_idindispoacteur_seq OWNED BY public.indisponibiliteacteur.idindispoacteur;


--
-- TOC entry 218 (class 1259 OID 16418)
-- Name: indisponibiliteplateau; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.indisponibiliteplateau (
    idindispo integer NOT NULL,
    plateauindispo integer,
    dateindispo date,
    motifindispo text DEFAULT 'en tournage'::text
);


ALTER TABLE public.indisponibiliteplateau OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16424)
-- Name: indisponibiliteplateau_idindispo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.indisponibiliteplateau_idindispo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indisponibiliteplateau_idindispo_seq OWNER TO postgres;

--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 219
-- Name: indisponibiliteplateau_idindispo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.indisponibiliteplateau_idindispo_seq OWNED BY public.indisponibiliteplateau.idindispo;


--
-- TOC entry 220 (class 1259 OID 16425)
-- Name: listetrue; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.listetrue AS
 SELECT DISTINCT ON (calendrier.dateexact, calendrier.heuredebutexact, calendrier.heurefinexact) calendrier.idcalendrier,
    calendrier.scene,
    calendrier.dateexact,
    calendrier.heuredebutexact,
    calendrier.heurefinexact
   FROM public.calendrier
  ORDER BY calendrier.dateexact, calendrier.heuredebutexact;


ALTER TABLE public.listetrue OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16429)
-- Name: scene; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scene (
    idscene integer NOT NULL,
    datescene date NOT NULL,
    nomscene character varying(100),
    heuredebut time(6) without time zone NOT NULL,
    heurefin time(6) without time zone NOT NULL,
    plateau integer NOT NULL,
    statusplanification integer DEFAULT 1,
    film integer,
    auteur integer NOT NULL
);


ALTER TABLE public.scene OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16433)
-- Name: listesceneordertrue; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.listesceneordertrue AS
 SELECT listetrue.idcalendrier,
    listetrue.scene,
    listetrue.dateexact,
    listetrue.heuredebutexact,
    listetrue.heurefinexact,
    scene.nomscene,
    scene.plateau
   FROM (public.listetrue
     JOIN public.scene ON ((listetrue.scene = scene.idscene)))
  ORDER BY scene.plateau, listetrue.dateexact, listetrue.heuredebutexact;


ALTER TABLE public.listesceneordertrue OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16437)
-- Name: plateau; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plateau (
    idplateau integer NOT NULL,
    nomplateau character varying(50) NOT NULL,
    lieu character varying(50) NOT NULL,
    longitude double precision,
    latitude double precision
);


ALTER TABLE public.plateau OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16440)
-- Name: plateau_idplateau_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.plateau_idplateau_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plateau_idplateau_seq OWNER TO postgres;

--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 224
-- Name: plateau_idplateau_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.plateau_idplateau_seq OWNED BY public.plateau.idplateau;


--
-- TOC entry 225 (class 1259 OID 16441)
-- Name: scene_idscene_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scene_idscene_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scene_idscene_seq OWNER TO postgres;

--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 225
-- Name: scene_idscene_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.scene_idscene_seq OWNED BY public.scene.idscene;


--
-- TOC entry 226 (class 1259 OID 16442)
-- Name: statusindispo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.statusindispo (
    idstatus integer NOT NULL,
    valeurstatus character varying
);


ALTER TABLE public.statusindispo OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16447)
-- Name: statusindispo_idstatus_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.statusindispo_idstatus_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.statusindispo_idstatus_seq OWNER TO postgres;

--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 227
-- Name: statusindispo_idstatus_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.statusindispo_idstatus_seq OWNED BY public.statusindispo.idstatus;


--
-- TOC entry 228 (class 1259 OID 16448)
-- Name: v_calendrier_plateau; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_calendrier_plateau AS
 SELECT c.idcalendrier,
    c.scene,
    c.dateexact,
    c.heuredebutexact,
    c.heurefinexact,
    p.idplateau
   FROM ((public.calendrier c
     JOIN public.scene s ON ((c.scene = s.idscene)))
     JOIN public.plateau p ON ((p.idplateau = s.plateau)));


ALTER TABLE public.v_calendrier_plateau OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16452)
-- Name: vue_acteurscene; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vue_acteurscene AS
 SELECT ac.idacteursceneaction,
    ac.acteur,
    ac.scene,
    ac.script,
    ac.duree,
    acteur.idacteur,
    acteur.nomacteur,
    acteur.genre,
    acteur.photoacteur,
    scene.idscene,
    scene.datescene,
    scene.nomscene AS nom,
    scene.heuredebut,
    scene.heurefin,
    scene.plateau,
    plateau.idplateau,
    plateau.nomplateau
   FROM (((public.acteursceneaction ac
     JOIN public.acteur ON ((ac.acteur = acteur.idacteur)))
     JOIN public.scene ON ((ac.scene = scene.idscene)))
     JOIN public.plateau ON ((scene.plateau = plateau.idplateau)));


ALTER TABLE public.vue_acteurscene OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16457)
-- Name: vue_calendrier; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vue_calendrier AS
 SELECT calendrier.idcalendrier,
    calendrier.scene,
    calendrier.dateexact,
    scene.idscene,
    scene.datescene,
    scene.nomscene AS nom,
    scene.heuredebut,
    scene.heurefin,
    scene.plateau,
    plateau.idplateau,
    plateau.nomplateau
   FROM ((public.calendrier
     JOIN public.scene ON ((calendrier.scene = scene.idscene)))
     JOIN public.plateau ON ((scene.plateau = plateau.idplateau)));


ALTER TABLE public.vue_calendrier OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16462)
-- Name: vue_scene; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vue_scene AS
 SELECT scene.idscene,
    scene.datescene,
    scene.nomscene AS nom,
    scene.heuredebut,
    scene.heurefin,
    scene.plateau,
    plateau.idplateau,
    plateau.nomplateau
   FROM (public.scene
     JOIN public.plateau ON ((scene.plateau = plateau.idplateau)));


ALTER TABLE public.vue_scene OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16542)
-- Name: vueacteurscene; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vueacteurscene AS
 SELECT ac.idacteursceneaction,
    acteur.idacteur,
    acteur.nomacteur,
    acteur.photoacteur,
    ac.script,
    ac.duree,
    scene.idscene,
    scene.nomscene,
    plateau.nomplateau,
    c.dateexact AS datescene
   FROM ((((public.acteursceneaction ac
     JOIN public.acteur ON ((ac.acteur = acteur.idacteur)))
     JOIN public.scene ON ((ac.scene = scene.idscene)))
     JOIN public.plateau ON ((scene.plateau = plateau.idplateau)))
     LEFT JOIN public.calendrier c ON ((c.scene = ac.scene)))
  ORDER BY ac.idacteursceneaction;


ALTER TABLE public.vueacteurscene OWNER TO postgres;

--
-- TOC entry 3242 (class 2604 OID 16471)
-- Name: acteur idacteur; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteur ALTER COLUMN idacteur SET DEFAULT nextval('public.acteur_idacteur_seq'::regclass);


--
-- TOC entry 3244 (class 2604 OID 16472)
-- Name: acteursceneaction idacteursceneaction; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteursceneaction ALTER COLUMN idacteursceneaction SET DEFAULT nextval('public.acteursceneaction_idacteursceneaction_seq'::regclass);


--
-- TOC entry 3255 (class 2604 OID 32807)
-- Name: auteur idauteur; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auteur ALTER COLUMN idauteur SET DEFAULT nextval('public.auteur_idauteur_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 16473)
-- Name: calendrier idcalendrier; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendrier ALTER COLUMN idcalendrier SET DEFAULT nextval('public.calendrier_idcalendrier_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 16474)
-- Name: film idfilm; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.film ALTER COLUMN idfilm SET DEFAULT nextval('public.film_idfilm_seq'::regclass);


--
-- TOC entry 3254 (class 2604 OID 32796)
-- Name: indisponibiliteacteur idindispoacteur; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.indisponibiliteacteur ALTER COLUMN idindispoacteur SET DEFAULT nextval('public.indisponibiliteacteur_idindispoacteur_seq'::regclass);


--
-- TOC entry 3248 (class 2604 OID 16475)
-- Name: indisponibiliteplateau idindispo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.indisponibiliteplateau ALTER COLUMN idindispo SET DEFAULT nextval('public.indisponibiliteplateau_idindispo_seq'::regclass);


--
-- TOC entry 3251 (class 2604 OID 16476)
-- Name: plateau idplateau; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plateau ALTER COLUMN idplateau SET DEFAULT nextval('public.plateau_idplateau_seq'::regclass);


--
-- TOC entry 3250 (class 2604 OID 16477)
-- Name: scene idscene; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scene ALTER COLUMN idscene SET DEFAULT nextval('public.scene_idscene_seq'::regclass);


--
-- TOC entry 3252 (class 2604 OID 16478)
-- Name: statusindispo idstatus; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statusindispo ALTER COLUMN idstatus SET DEFAULT nextval('public.statusindispo_idstatus_seq'::regclass);


--
-- TOC entry 3433 (class 0 OID 16395)
-- Dependencies: 209
-- Data for Name: acteur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.acteur VALUES (0, ' ', ' ', ' ');
INSERT INTO public.acteur VALUES (1, 'rakoto', 'homme', 'acteur001.jpg');
INSERT INTO public.acteur VALUES (2, 'rasoa', 'femme', 'acteur002.jpg');
INSERT INTO public.acteur VALUES (3, 'rabe', 'homme', 'acteur003.jpg');
INSERT INTO public.acteur VALUES (4, 'randria', 'homme', 'acteur004.jpg');
INSERT INTO public.acteur VALUES (5, 'marie', 'femme', 'acteur005.jpg');


--
-- TOC entry 3435 (class 0 OID 16399)
-- Dependencies: 211
-- Data for Name: acteurscene; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.acteurscene VALUES (0, 1, 1);
INSERT INTO public.acteurscene VALUES (1, 1, 2);
INSERT INTO public.acteurscene VALUES (2, 1, 3);
INSERT INTO public.acteurscene VALUES (1, 1, 4);
INSERT INTO public.acteurscene VALUES (3, 1, 5);
INSERT INTO public.acteurscene VALUES (0, 1, 6);
INSERT INTO public.acteurscene VALUES (3, 1, 7);
INSERT INTO public.acteurscene VALUES (0, 2, 8);
INSERT INTO public.acteurscene VALUES (4, 2, 9);
INSERT INTO public.acteurscene VALUES (5, 2, 10);
INSERT INTO public.acteurscene VALUES (0, 3, 11);
INSERT INTO public.acteurscene VALUES (3, 3, 12);
INSERT INTO public.acteurscene VALUES (2, 3, 13);
INSERT INTO public.acteurscene VALUES (3, 3, 14);
INSERT INTO public.acteurscene VALUES (1, 3, 15);
INSERT INTO public.acteurscene VALUES (3, 3, 16);
INSERT INTO public.acteurscene VALUES (2, 4, 17);
INSERT INTO public.acteurscene VALUES (0, 4, 18);
INSERT INTO public.acteurscene VALUES (2, 4, 19);
INSERT INTO public.acteurscene VALUES (3, 4, 20);
INSERT INTO public.acteurscene VALUES (1, 6, 21);
INSERT INTO public.acteurscene VALUES (2, 6, 22);
INSERT INTO public.acteurscene VALUES (1, 7, 23);
INSERT INTO public.acteurscene VALUES (1, 14, 24);
INSERT INTO public.acteurscene VALUES (3, 14, 25);
INSERT INTO public.acteurscene VALUES (5, 14, 26);
INSERT INTO public.acteurscene VALUES (1, 8, 27);
INSERT INTO public.acteurscene VALUES (2, 8, 28);
INSERT INTO public.acteurscene VALUES (3, 8, 29);
INSERT INTO public.acteurscene VALUES (5, 8, 30);
INSERT INTO public.acteurscene VALUES (5, 8, 31);
INSERT INTO public.acteurscene VALUES (1, 16, 32);
INSERT INTO public.acteurscene VALUES (2, 16, 33);
INSERT INTO public.acteurscene VALUES (3, 16, 34);


--
-- TOC entry 3436 (class 0 OID 16402)
-- Dependencies: 212
-- Data for Name: acteursceneaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.acteursceneaction VALUES (1, 0, 1, 'Varavarana mivoha', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (2, 1, 1, 'Miditra ny trano, miakatra tohatra', '00:03:00');
INSERT INTO public.acteursceneaction VALUES (3, 2, 1, 'Miarahaba anao', '00:03:00');
INSERT INTO public.acteursceneaction VALUES (4, 1, 1, 'Salama eh, manahoana ianareo aty', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (5, 3, 1, 'Hay misy vahiny ato, manahoana eh', '00:04:00');
INSERT INTO public.acteursceneaction VALUES (6, 0, 1, 'Mande ny vaovao eo amin''ny fahitalavitra', '00:06:00');
INSERT INTO public.acteursceneaction VALUES (7, 3, 1, 'Tena enjana ity tsy fandriampahalemana etsy sy eroa ity', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (8, 0, 2, 'Fiara mande mafy, mirimorimo ka nandona mpandeha antongotra satria tsy tafajanona', '00:10:00');
INSERT INTO public.acteursceneaction VALUES (9, 4, 2, 'Fa ianareo ve ramatoa ity tsy mijery fa toerana fiampitana eto nefa tsy mijanona mintsy ianareo raha tsy nandona olona', '00:05:00');
INSERT INTO public.acteursceneaction VALUES (10, 5, 2, 'Tena azafady indrindra re tompoko fa maika amonjy marary any amin''ny hopitaly za de tsy itako izy', '00:05:00');
INSERT INTO public.acteursceneaction VALUES (11, 0, 3, 'Masoandro miposaka sy akoho maneno', '00:05:00');
INSERT INTO public.acteursceneaction VALUES (12, 3, 3, 'Mifohaza amin''izay mahandro sakafo fa maraina ny andro ooh', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (13, 2, 3, 'Tena mbola kamo be za fa alina loatra anie isika vao natory eh', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (14, 3, 3, 'Mifohaza fa mbola ande hiasa anie isika eh', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (15, 1, 3, 'Fa inona izato kotaba vao maraina zato reto ah, tsisy hatoriana maraina ianareo ', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (16, 3, 3, 'Maika itady vola r''ise fa sarotra anie ny fitadiavana amin''izao eh', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (17, 2, 4, 'Tena tsy misy rano akory aza ato fa tapaka ny jirama alina kou', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (18, 0, 4, 'Sinibe tsisy rano', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (19, 2, 4, 'Mitsangana aloha ianareo mandeha maka rano fa tsy misy andrahoana sakafou ato', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (20, 3, 4, 'Ho avy eh', '00:02:00');
INSERT INTO public.acteursceneaction VALUES (21, 1, 6, 'nandeha i rakoto', '00:30:00');
INSERT INTO public.acteursceneaction VALUES (22, 2, 6, 'nanantona azy i rasoa ', '00:15:00');
INSERT INTO public.acteursceneaction VALUES (23, 1, 7, 'Mandehana amin''izay enao mandeha any hoy i rakoto no sady tezitra izy', '00:00:59');
INSERT INTO public.acteursceneaction VALUES (24, 2, 8, 'scrt', '05:06:00');


--
-- TOC entry 3454 (class 0 OID 32804)
-- Dependencies: 237
-- Data for Name: auteur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.auteur VALUES (1, 'Auteur1');
INSERT INTO public.auteur VALUES (2, 'Auteur2');


--
-- TOC entry 3438 (class 0 OID 16408)
-- Dependencies: 214
-- Data for Name: calendrier; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.calendrier VALUES (151, 8, '2023-03-22', '09:00:00', '10:00:00');
INSERT INTO public.calendrier VALUES (152, 13, '2023-03-22', '10:00:00', '12:00:00');
INSERT INTO public.calendrier VALUES (153, 12, '2023-03-22', '12:00:00', '16:00:00');


--
-- TOC entry 3440 (class 0 OID 16412)
-- Dependencies: 216
-- Data for Name: film; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.film VALUES (1, 'Film1', 'Rakoto', 'Film gasy t', 'blog1.jpg');
INSERT INTO public.film VALUES (2, 'Film2', 'JohnTra', 'Film Tsy milay', 'blog2.jpg');
INSERT INTO public.film VALUES (3, 'Test', 'Test3', '                                                        
                     film test                               ', 'banner5.jpg');


--
-- TOC entry 3451 (class 0 OID 32789)
-- Dependencies: 234
-- Data for Name: indisponibiliteacteur; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3442 (class 0 OID 16418)
-- Dependencies: 218
-- Data for Name: indisponibiliteplateau; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.indisponibiliteplateau VALUES (51, 1, '2023-03-14', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (52, 1, '2023-03-14', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (53, 1, '2023-03-14', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (54, 1, '2023-03-21', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (55, 2, '2023-03-21', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (56, 2, '2023-03-21', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (57, 2, '2023-03-21', 'Planned in filming');
INSERT INTO public.indisponibiliteplateau VALUES (58, 3, '2023-03-21', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (59, 3, '2023-03-21', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (60, 3, '2023-03-21', NULL);
INSERT INTO public.indisponibiliteplateau VALUES (61, 3, '2023-03-21', NULL);


--
-- TOC entry 3445 (class 0 OID 16437)
-- Dependencies: 223
-- Data for Name: plateau; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.plateau VALUES (3, 'plateau3', 'Analakely', -18.907456984067608, 47.525069449576684);
INSERT INTO public.plateau VALUES (1, 'plateau1', 'Andoharanofotsy', -18.98604484915179, 47.53294104377077);
INSERT INTO public.plateau VALUES (2, 'plateau2', 'Tanjombato', -18.95436984661374, 47.52564291832568);


--
-- TOC entry 3444 (class 0 OID 16429)
-- Dependencies: 221
-- Data for Name: scene; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.scene VALUES (2, '2023-03-01', 'scene2', '14:00:00', '16:00:00', 2, 1, 2, 1);
INSERT INTO public.scene VALUES (8, '2023-03-16', 'scenetanjona1', '08:00:00', '10:00:00', 1, 2, 1, 2);
INSERT INTO public.scene VALUES (9, '2023-03-16', 'scenetanjona2', '08:00:00', '10:00:00', 2, 2, 2, 1);
INSERT INTO public.scene VALUES (10, '2023-03-16', 'scenetanjona3', '08:00:00', '10:00:00', 3, 3, 1, 2);
INSERT INTO public.scene VALUES (11, '2023-03-16', 'scenetanjona4', '08:00:00', '10:00:00', 3, 3, 2, 1);
INSERT INTO public.scene VALUES (12, '2023-03-16', 'scenetanjona5', '08:00:00', '10:00:00', 2, 4, 1, 2);
INSERT INTO public.scene VALUES (13, '2023-03-16', 'scenetanjona6', '08:00:00', '10:00:00', 3, 4, 2, 1);
INSERT INTO public.scene VALUES (1, '2023-03-01', 'scene1', '08:00:00', '10:00:00', 1, 1, 1, 2);
INSERT INTO public.scene VALUES (7, '2023-03-17', 'Dero Sarle', '16:34:00', '18:34:00', 1, 1, 2, 1);
INSERT INTO public.scene VALUES (4, '2023-03-03', 'scene4', '14:00:00', '16:00:00', 1, 1, 1, 2);
INSERT INTO public.scene VALUES (6, '2023-03-17', 'Johnsdf Doesdf', '10:30:00', '12:30:00', 1, 1, 2, 1);
INSERT INTO public.scene VALUES (3, '2023-03-02', 'scene3', '08:00:00', '10:00:00', 2, 1, 1, 2);
INSERT INTO public.scene VALUES (14, '2023-03-20', 'Scene Tets', '09:40:00', '13:45:00', 1, 1, 3, 1);
INSERT INTO public.scene VALUES (15, '2023-03-20', 'Scene Tets', '09:40:00', '13:45:00', 1, 1, 3, 1);
INSERT INTO public.scene VALUES (16, '2023-03-22', 'Scene Tets', '09:24:00', '22:24:00', 1, 1, 3, 1);


--
-- TOC entry 3448 (class 0 OID 16442)
-- Dependencies: 226
-- Data for Name: statusindispo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.statusindispo VALUES (1, 'Created');
INSERT INTO public.statusindispo VALUES (2, 'In Writing');
INSERT INTO public.statusindispo VALUES (3, 'Finish');
INSERT INTO public.statusindispo VALUES (4, 'Planified on filming');


--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 210
-- Name: acteur_idacteur_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.acteur_idacteur_seq', 1, false);


--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 213
-- Name: acteursceneaction_idacteursceneaction_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.acteursceneaction_idacteursceneaction_seq', 24, true);


--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 236
-- Name: auteur_idauteur_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auteur_idauteur_seq', 1, false);


--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 215
-- Name: calendrier_idcalendrier_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calendrier_idcalendrier_seq', 153, true);


--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 217
-- Name: film_idfilm_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.film_idfilm_seq', 7, true);


--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 235
-- Name: indisponibiliteacteur_idindispoacteur_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.indisponibiliteacteur_idindispoacteur_seq', 1, false);


--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 219
-- Name: indisponibiliteplateau_idindispo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.indisponibiliteplateau_idindispo_seq', 61, true);


--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 224
-- Name: plateau_idplateau_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.plateau_idplateau_seq', 1, false);


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 225
-- Name: scene_idscene_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.scene_idscene_seq', 1, false);


--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 233
-- Name: seq_acteurscene; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seq_acteurscene', 34, true);


--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 227
-- Name: statusindispo_idstatus_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.statusindispo_idstatus_seq', 1, false);


--
-- TOC entry 3257 (class 2606 OID 16480)
-- Name: acteur acteur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteur
    ADD CONSTRAINT acteur_pkey PRIMARY KEY (idacteur);


--
-- TOC entry 3259 (class 2606 OID 16482)
-- Name: acteurscene acteurscene_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteurscene
    ADD CONSTRAINT acteurscene_pkey PRIMARY KEY (idacteurscene);


--
-- TOC entry 3261 (class 2606 OID 16484)
-- Name: acteursceneaction acteursceneaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteursceneaction
    ADD CONSTRAINT acteursceneaction_pkey PRIMARY KEY (idacteursceneaction);


--
-- TOC entry 3275 (class 2606 OID 32811)
-- Name: auteur auteur_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auteur
    ADD CONSTRAINT auteur_pk PRIMARY KEY (idauteur);


--
-- TOC entry 3263 (class 2606 OID 16486)
-- Name: calendrier calendrier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendrier
    ADD CONSTRAINT calendrier_pkey PRIMARY KEY (idcalendrier);


--
-- TOC entry 3265 (class 2606 OID 16488)
-- Name: film film_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_pk PRIMARY KEY (idfilm);


--
-- TOC entry 3267 (class 2606 OID 16490)
-- Name: indisponibiliteplateau indisponibiliteplateau_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.indisponibiliteplateau
    ADD CONSTRAINT indisponibiliteplateau_pk PRIMARY KEY (idindispo);


--
-- TOC entry 3271 (class 2606 OID 16492)
-- Name: plateau plateau_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plateau
    ADD CONSTRAINT plateau_pkey PRIMARY KEY (idplateau);


--
-- TOC entry 3269 (class 2606 OID 16494)
-- Name: scene scene_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scene
    ADD CONSTRAINT scene_pkey PRIMARY KEY (idscene);


--
-- TOC entry 3273 (class 2606 OID 16496)
-- Name: statusindispo statusindispo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statusindispo
    ADD CONSTRAINT statusindispo_pkey PRIMARY KEY (idstatus);


--
-- TOC entry 3276 (class 2606 OID 16497)
-- Name: acteurscene acteurscene_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteurscene
    ADD CONSTRAINT acteurscene_fk FOREIGN KEY (acteur) REFERENCES public.acteur(idacteur);


--
-- TOC entry 3277 (class 2606 OID 16502)
-- Name: acteurscene acteurscene_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteurscene
    ADD CONSTRAINT acteurscene_fk_1 FOREIGN KEY (scene) REFERENCES public.scene(idscene);


--
-- TOC entry 3285 (class 2606 OID 32812)
-- Name: scene auteur_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scene
    ADD CONSTRAINT auteur_fk FOREIGN KEY (auteur) REFERENCES public.auteur(idauteur);


--
-- TOC entry 3278 (class 2606 OID 16507)
-- Name: acteursceneaction fkacteurscen346883; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteursceneaction
    ADD CONSTRAINT fkacteurscen346883 FOREIGN KEY (acteur) REFERENCES public.acteur(idacteur);


--
-- TOC entry 3279 (class 2606 OID 16512)
-- Name: acteursceneaction fkacteurscen374866; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acteursceneaction
    ADD CONSTRAINT fkacteurscen374866 FOREIGN KEY (scene) REFERENCES public.scene(idscene);


--
-- TOC entry 3280 (class 2606 OID 16517)
-- Name: calendrier fkcalendrier588466; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendrier
    ADD CONSTRAINT fkcalendrier588466 FOREIGN KEY (scene) REFERENCES public.scene(idscene);


--
-- TOC entry 3282 (class 2606 OID 16522)
-- Name: scene fkscene390276; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scene
    ADD CONSTRAINT fkscene390276 FOREIGN KEY (plateau) REFERENCES public.plateau(idplateau);


--
-- TOC entry 3286 (class 2606 OID 32797)
-- Name: indisponibiliteacteur indisponibiliteacteur_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.indisponibiliteacteur
    ADD CONSTRAINT indisponibiliteacteur_fk FOREIGN KEY (acteurindispo) REFERENCES public.acteur(idacteur);


--
-- TOC entry 3281 (class 2606 OID 16527)
-- Name: indisponibiliteplateau indisponibiliteplateau_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.indisponibiliteplateau
    ADD CONSTRAINT indisponibiliteplateau_fk FOREIGN KEY (plateauindispo) REFERENCES public.plateau(idplateau);


--
-- TOC entry 3283 (class 2606 OID 16532)
-- Name: scene scene_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scene
    ADD CONSTRAINT scene_fk FOREIGN KEY (statusplanification) REFERENCES public.statusindispo(idstatus);


--
-- TOC entry 3284 (class 2606 OID 16537)
-- Name: scene scene_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scene
    ADD CONSTRAINT scene_fk1 FOREIGN KEY (film) REFERENCES public.film(idfilm);


-- Completed on 2023-03-21 17:35:14

--
-- PostgreSQL database dump complete
--

