<!DOCTYPE html>
<html lang="en">


    <!-- index-414:33-->
    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibz - Conference &amp; Event HTML Template</title>



        <!-- CSS
              ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->

    </head>

    <body class="body-color">
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/listeindispo">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <!-- banner start-->
            <section class="hero-area content-left">
                <div class="banner-item" style="background-image: url(${pageContext.request.contextPath}/ressources/images/hero_area/1qfj4mo5dn78hkb9a4mumsynb0-ojwfqkj-qozykswzgowaalow9kkdpe9bdo1r4-1.jpg)">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="banner-content-wrap">
                                    <img class="title-shap-img" src="${pageContext.request.contextPath}/ressources/images/shap/title-white.png" alt="">
                                    <h1 class="banner-title wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="700ms">
                                        Plan your schedule movies with us.
                                    </h1>
                                    <p class="banner-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">5 to 7 June
                                        2019, Waterfront Conference,
                                        London, United Kingdom</p>
                                    <!-- Countdown end -->
                                    <div class="banner-btn wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="800ms">
                                        <!-- <a href="#" class="btn">Next</a> -->
                                    </div>

                                </div>
                                <!-- Banner content wrap end -->
                            </div><!-- col end-->
                        </div><!-- row end-->
                    </div>
                    <!-- Container end -->
                </div>
            </section>
            <!-- banner end-->






            <!-- Javascript Files
                  ================================================== -->
            <!-- initialize jQuery Library -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>

            <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
            <!-- Bootstrap jQuery -->
            <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
            <!-- Counter -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
            <!-- Countdown -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
            <!-- magnific-popup -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
            <!-- carousel -->
            <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
            <!-- Waypoints -->
            <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>

            <!-- isotop -->
            <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>

            <!-- Template custom -->
            <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>

        </div>
        <!-- Body inner end -->
    </body>


    <!-- index-415:19-->
</html>