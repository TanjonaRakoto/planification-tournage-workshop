<%@page import="model.Calendrier"%>
<%@page import="java.util.Map"%>
<%@page import="java.sql.Date"%>
<%@page import="java.util.List"%>
<%
    Map<Date, List<Calendrier>> calendrier = (Map<Date, List<Calendrier>>) request.getAttribute("bookMap");
%>

<!DOCTYPE html>
<html lang="en">


    <!-- schedule-tab-117:31-->

    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Conference &amp; Event HTML Template</title>

        <!-- CSS
              ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->

    </head>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/trayindisponible">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area" style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2>Schedule</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Exhibit /</a>
                            </li>
                            <li>
                                Schedule Tab
                            </li>
                        </ol>
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->

            <section class="ts-schedule">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="ts-schedule-content">
                                <h2 class="column-title">
                                    <span>Schedule Details</span>
                                    Information of Event Schedules
                                </h2>
                                <p>World is committed to making participation in the event a harassment free experience for
                                    everyone, regardless of level of experience, gender, gender identity and expression
                                </p>
                            </div>
                        </div><!-- col end-->
                        <!-- 
                        <div class="col-lg-6">
                            <div class="ts-schedule-info mb-70">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="active" title="Click Me" href="#date1" role="tab" data-toggle="tab">
                                            <h3>5th June</h3>
                                            <span>Friday</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="#date2" title="Click Me" role="tab" data-toggle="tab">
                                            <h3>6th June</h3>
                                            <span>Saturday</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="#date3" title="Click Me" role="tab" data-toggle="tab">
                                            <h3>7th June</h3>
                                            <span>Sunday</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="#date4" title="Click Me" role="tab" data-toggle="tab">
                                            <h3>2th June</h3>
                                            <span>Monday</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="#date4" title="Click Me" role="tab" data-toggle="tab">
                                            <h3>8th June</h3>
                                            <span>Thursday</span>
                                        </a>
                                    </li>
                                </ul>                                
                            </div>
                        </div> -->
                    </div><!-- row end-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <table class="table table-bordered">
                                    <tbody>

                                        <%
                                            for (int i = 0; i < calendrier.size(); i++) {
                                        %>

                                        <tr>
                                            <th>
                                                <div>
                                                    <p><%= calendrier.keySet().toArray()[i]%></p>                                                                                   
                                                </div>
                                            </th>
                                            <%
                                                List<Calendrier> lis = calendrier.get((Date) calendrier.keySet().toArray()[i]);
                                                for (Calendrier elem : lis) {
                                            %>
                                            <td>
                                                <div class="bg-warning" style="border-radius: 6px;">
                                                    <div class="text-center">
                                                        <h6 class="pt-1"><%= elem.getHeuredebutexact()%>  - <%= elem.getHeurefinexact()%> </h6>
                                                        <h5><%= elem.getScene()%></h5>
                                                        <p><%= elem.getDateexact()%></p>
                                                        <!-- <button data-toggle="modal" data-target="#modify"><i class="fa fa-edit"></i></button> -->
                                                    </div>
                                                </div>
                                            </td> 
                                            <%
                                                    //}
                                                }
                                            %>

                                        </tr>

                                        <%
                                            }
                                        %>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div><!-- container end-->
            </section>

            <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>

            <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
            <!-- Bootstrap jQuery -->
            <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
            <!-- Counter -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
            <!-- Countdown -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
            <!-- magnific-popup -->
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
            <!-- carousel -->
            <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
            <!-- Waypoints -->
            <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
            <!-- isotop -->
            <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>

            <!-- Template custom -->
            <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>
        </div>
        <!-- Body inner end -->
    </body>


    <!-- schedule-tab-117:31-->

</html>