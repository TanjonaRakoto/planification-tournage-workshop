
<%@page import="java.util.List"%>
<%@page import="model.Scene"%>
<%
    List<Scene> sceneNoPlaned = (List<Scene>) request.getAttribute("sceneNoPlaned");
%>
<!DOCTYPE html>
<html lang="en">


    <!-- schedule-list17:31-->

    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Select scene</title>

        <!-- CSS
             ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/bootstrap/css/bootstrap.min.css">
        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->
    </head>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/trayindisponible">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area" style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2> Scene list unplanned</h2>
                        <ol class="breadcrumb">
                            <li>
                                Select the scenes you want to plan.
                            </li>
                        </ol>
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->

            <section class="ts-schedule">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="section-title">
                                For new planning
                            </h2>
                        </div><!-- col end-->
                    </div><!-- row end-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content schedule-tabs">
                                <div role="tabpanel" class="tab-pane active" id="date3">

                                    <form action="listordertrue">
                                        <%
                                            for (Scene elem : sceneNoPlaned) {
                                        %>

                                        <div class="schedule-listing">
                                            <!-- style="background-color: cornflowerblue;" -->
                                            <div class="schedule-slot-time" id="schedule<%= elem.getIdscene()%>">
                                                <p class="shedule-slot-time">Begin: <%= elem.getHeuredebut()%> &nbsp;&nbsp; End: <%= elem.getHeurefin()%></p>
                                                <p class="shedule-slot-time">Date: <%= elem.getDatescene()%></p>
                                            </div>
                                            <div class="schedule-slot-info">
                                                <div class="schedule-slot-info-content" style="display: flex;">
                                                    <div style=" padding: 30px;">
                                                        Nom scene 
                                                        <h3 class="schedule-slot-title"><%= elem.getNomscene()%>
                                                        </h3>
                                                    </div>
                                                    <div style=" padding: 30px;">

                                                        Nom Plateau
                                                        <h4 class="schedule-slot-title">
                                                            <%= elem.getPlateau().getNomplateau()%>
                                                        </h4>
                                                    </div>
                                                    <div style=" padding: 30px;">

                                                        Lieu
                                                        <h4 class="schedule-slot-title">
                                                            <%= elem.getPlateau().getLieu()%>
                                                        </h4>
                                                    </div>                                                    
                                                </div>
                                                <div>
                                                    <button type="button" onclick="addSelect('<%= elem.getIdscene()%>', <%= elem.getIdscene()%>, this.id)" class="btn"
                                                            name="" id="button<%= elem.getIdscene()%>" >Select</button>
                                                </div>
                                            </div>
                                        </div>                                    

                                        <%
                                            }
                                        %>

                                        <div class="schedule-listing-btn">
                                            <input type="hidden" name="idscene[]" id="idscene" />
                                            <button class="btn" type="submit" data-toggle="modal"
                                                    data-target="#dateplan">Next</button>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- container end-->
                <div class="speaker-shap">
                    <img class="shap2" src="images/shap/home_schedule_memphis1.png" alt="">
                    <img class="shap1" src="images/shap/home_schedule_memphis2.png" alt="">
                </div>
            </section>
            <script>
                var listIdScene = []
                function addSelect(idscene, index, id) {
                    var repere = document.getElementById(id).innerHTML;

                    if (repere !== "Cancel") {
                        listIdScene.push(idscene);
                        document.getElementById(id).innerHTML = "Cancel"
                        document.getElementById(id).style.backgroundColor = "#bababa"
                        document.getElementById("schedule" + index).style.backgroundColor = "#73f279";
                    } else {
                        listIdScene.splice(listIdScene.indexOf(idscene), 1);
                        document.getElementById(id).innerHTML = "Select"
                        document.getElementById(id).style.backgroundColor = "#ff007a"
                        document.getElementById(id).style.color = "white"
                        document.getElementById("schedule" + index).style.backgroundColor = "#ff007a";
                    }

                    for (let i = 0; i < listIdScene.length; i++) {
                        const element = listIdScene[i];
                        console.log(element);
                    }

                    // affection valeur idscene (input:hidden)
                    document.getElementById("idscene").value = listIdScene;

                }
            </script>

            <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>
        </div>
    </body>

</html>