<%@ page import="model.*" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">


    <!-- venue16:34-->

    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Conference &amp; Event HTML Template</title>


        <!-- CSS
             ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/fonts/font-awesome.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

    </head>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/trayindisponible">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area"
                 style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2>Creating a scene</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Tray /</a>
                            </li>
                            <li>
                                Actors
                            </li>
                        </ol>
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->





            <!-- ts map direction start-->
            <section class="ts-map-direction wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-6 ">
                            <div class="ts-grid-item">
                                <div class="grid-item">
                                    <img src="${pageContext.request.contextPath}/ressources/images/hero_area/VIETNAM-FILM-CREW-EQUIPMENT-RENTAL-1024x683.jpg" alt=""/>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 offset-lg-1">
                            <h2 class="column-title">
                                <span>Choice</span>
                                Actors
                            </h2>

                            <div class="ts-map-tabs">
                                <ul class="nav" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link">Scene</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#buzz" role="tab" data-toggle="tab">Actors</a>
                                    </li>
                                </ul>
                                <form action="action" method="post" class="form-control-feedback">
                                    <!-- Tab panes -->
                                    <div class="tab-content direction-tabs">
                                        <div role="tabpanel" class="tab-pane active" id="buzz">
                                            <div class="direction-tabs-content">
                                                <h3>Choose the actors needed for this scene</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="contact-form">
                                                            <%
                                                                List<Acteur> acteurs = (List<Acteur>) request.getAttribute("acteurs");
                                                            %>
                                                            <select class="form-control"
                                                                    onchange="ajout(this.value, this.options[this.selectedIndex].textContent)">
                                                                <% for (Acteur a : acteurs) {%>
                                                                <option value="<%= a.getIdacteur()%>"><%= a.getNomacteur()%></option>
                                                                <% }%>
                                                            </select>
                                                        </div>
                                                        <input type="hidden" name="acteur[]" id="actors" />
                                                        <button class="btn btn-sm btn-outline-success"
                                                                style="border: none; margin-top: 0.8cm;">Next</button>
                                                    </div>
                                                    <div class="col">
                                                        <ul id="listActors-li">
                                                        </ul>
                                                    </div>
                                                </div><!-- row end-->
                                            </div><!-- direction tabs end-->
                                        </div>
                                </form>
                            </div>
                        </div><!-- map tabs end-->
                    </div><!-- col end-->
                </div><!-- col end-->
            </section>
        </div>

        <script>
            var listActors = []
            var li_ctors = document.getElementById("listActors-li");
            function ajout(str, opt) {
                listActors.push(str);
                li_ctors.innerHTML += "<li>" + opt + "</li>"
                document.getElementById("actors").value = listActors;
            }
        </script>

        <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>

        <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
        <!-- Bootstrap jQuery -->
        <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
        <!-- Counter -->
        <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
        <!-- Countdown -->
        <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
        <!-- magnific-popup -->
        <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
        <!-- carousel -->
        <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
        <!-- Waypoints -->
        <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
        <!-- isotop -->
        <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>

        <!-- Template custom -->
        <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>

    </div>
    <!-- Body inner end -->
</body>


<!-- venue17:31-->

</html>