<%@ page import="model.*" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">


    <!-- schedule-list17:31-->
    <%
        List<Scene> scenes = (List<Scene>) request.getAttribute("scenes");
    %>
    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Conference &amp; Event HTML Template</title>

        <!-- CSS
              ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/bootstrap/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->

    </head>

    <body onload="changeMood(<%= request.getAttribute("filtre")%>, <%= scenes.size()%>)">
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a href="${pageContext.request.contextPath}" class="">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes">Scene List</a></li>
                                        <li><a href="tray-list.html">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area" style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2> Scene List</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Exhibit /</a>
                            </li>
                            <li>
                                Scene List
                            </li>
                        </ol>
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->

            <section class="ts-schedule">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="section-title">
                                <span>Info</span>
                                <label id="title<%= request.getAttribute("filtre")%>">Film in production</label>
                            </h2>
                        </div><!-- col end-->
                    </div><!-- row end-->
                    <div class="row" style="margin-top: 0;">
                        <div class="col-lg-12 text-sm-center">
                            <div class="ts-blog form-group">
                                <form action="filtre" method="post" class="form-control-sm">
                                    <div class="position-absolute">
                                        <select name="filter" id="filter" class="form-control" style="height: 50px;">
                                            <% List<Statusindispo> listfiltre = (List<Statusindispo>) request.getAttribute("listefiltre");%>
                                            <option value="<%= listfiltre.get(0).getIdstatus()%>" id="<%= listfiltre.get(0).getIdstatus()%>" selected><%= listfiltre.get(0).getValeurstatus()%></option>
                                            <option value="<%= listfiltre.get(1).getIdstatus()%>" id="<%= listfiltre.get(1).getIdstatus()%>" style="background-color: aquamarine;"><%= listfiltre.get(1).getValeurstatus()%></option>
                                            <option value="<%= listfiltre.get(2).getIdstatus()%>" id="<%= listfiltre.get(2).getIdstatus()%>" style="background-color: #e7015e;"><%= listfiltre.get(2).getValeurstatus()%></option>
                                            <option value="<%= listfiltre.get(3).getIdstatus()%>" id="<%= listfiltre.get(3).getIdstatus()%>" style="background-color: bisque;"><%= listfiltre.get(3).getValeurstatus()%></option>
                                        </select>
                                    </div>
                                    <div style="margin-left: -15cm;">
                                        <button class=" btn btn-sm" >Filter &nbsp;<i class="fa fa-arrow-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- col end-->
                    </div><!-- row end-->

                    <!-- ito no bouclena -->
                    <div class="row">

                        <%
                            int i = 0;
                            for (Scene elem : scenes) {
                        %>                        

                        <div class="col-lg-4 wow post fadeInUp mt-1" data-wow-duration="1.5s" data-wow-delay="400ms">
                            <div class="card ">
                                <div id="body-card<%=i%>" class="card-body">
                                    <div class="entry-header mt-0">
                                        <h2 class="entry-title">
                                            <a id="scene-name<%=i%>" href="#" style="color: #e7015e;"><%= elem.getNomscene()%></a>
                                        </h2>
                                    </div><!-- header end -->
                                    <div class="post-meta">
                                        <span class="post-author">
                                            <a id="time<%=i%>" href="#" style="color: #e7015e;">Time&nbsp;: <%= elem.getHeuredebut()%> - <%= elem.getHeurefin()%></a>
                                        </span>
                                    </div>
                                    <div class="post-meta">
                                        <span class="post-author">
                                            <a id="date<%=i%>" href="#" style="color: #e7015e;">Date&nbsp;: <%= elem.getDatescene()%></a>
                                        </span>
                                    </div>

                                    <div class="entry-content">
                                        <p>Tray name : <%= elem.getPlateau().getNomplateau()%> </p>
                                        <p>Location : <%= elem.getPlateau().getLieu()%> </p>
                                    </div>


                                </div><!-- post-body end -->
                                <div class="card-footer">
                                    <a id="bnt-modify<%=i%>" href="${pageContext.request.contextPath}/scene/<%= elem.getIdscene()%>" class="btn-link float-left">Modify <i class="icon icon-edit"></i></a>
                                    <a href="${pageContext.request.contextPath}/detail?idscene=<%= elem.getIdscene()%>" class="btn-link float-right">Read More <i class="icon icon-arrow-right"></i></a>
                                </div>
                            </div>
                        </div><!-- col end-->

                        <% i++;
                            }%>


                    </div><!-- row end-->

                    <!-- k'hatreto -->



                    <div class="schedule-listing-btn" id="btn-planified">
                        <button class="btn" type="button" data-toggle="modal" data-target="#dateplan">Plan the
                            Scene</button>
                    </div>

                </div><!-- container end-->
            </section>
            <div id="dateplan" class="modal fade" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Date for planning</h4><button class="btn-close" type="button" data-dismiss="modal"
                                                              aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="selectscene" method="post">
                                <div class="form-group mb-3">
                                    <div class="input-group">
                                        <span class="text-primary input-group-text"><i
                                                class="fa fa-calendar-times-o"></i></span><input class="form-control" type="date" name="datedebut"
                                                                                         required placeholder="Start date" />
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="input-group"><span class="text-primary input-group-text"><i
                                                class="fa fa-calendar-times-o"></i></span><input class="form-control" type="date" name="datefin"
                                                                                         required placeholder="End date" /></div>
                                </div>
                                <div class="form-group mb-3"><button class="btn btn-sm" style="width: 100%;"
                                                                     type="submit">Next</button></div>
                            </form>
                            <hr style="background-color: #bababa;" />
                        </div>
                    </div>
                </div>
            </div>

            <script>

                function changeMood(index, size) {

                    console.log(index);
                    console.log(size);

                    var title = document.getElementById("title" + index);
                    var idOption = document.getElementById("" + index);
                    title.innerHTML = idOption.innerHTML;
                    var btnPlanified = document.getElementById("btn-planified");
                    btnPlanified.style.display = "none";
                    for (var i = 0; i < size; i++) {

                        console.log(i + "-----");

                        var bodyCard = document.getElementById("body-card" + i);
                        var sceneName = document.getElementById("scene-name" + i);
                        var time = document.getElementById("time" + i);
                        var date = document.getElementById("date" + i);
                        var btnModify = document.getElementById("bnt-modify" + i);


                        if (index === 1) {
                            bodyCard.style.backgroundColor = "white";
                            sceneName.style.color = "e7015e";
                            time.style.color = "e7015e";
                            date.style.color = "e7015e";
                            btnModify.style.display = "none";
                            idOption.setAttribute("selected", "");
                            title.style.color = "e7015e";

                        } else if (index === 2) {
                            bodyCard.style.backgroundColor = "aquamarine";
                            sceneName.style.color = "green";
                            time.style.color = "green";
                            date.style.color = "green";
                            btnModify.style.display = "block";
                            idOption.setAttribute("selected", "");
                            title.style.color = "green";
                        } else if (index === 4) {
                            bodyCard.style.backgroundColor = "bisque";
                            sceneName.style.color = "blue";
                            time.style.color = "blue";
                            date.style.color = "blue";
                            btnModify.style.display = "none";
                            idOption.setAttribute("selected", "");
                            title.style.color = "blue";
                        } else {
                            bodyCard.style.backgroundColor = "#e7015e";
                            sceneName.style.color = "white";
                            time.style.color = "white";
                            date.style.color = "white";
                            btnModify.style.display = "none";
                            idOption.setAttribute("selected", "");
                            btnPlanified.style.display = "block";
                        }
                    }
                }

            </script>

            <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>
        </div>
    </body>

</html>