<%@page import="model.Film"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">


    <!-- venue16:34-->

    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Conference &amp; Event HTML Template</title>


        <!-- CSS
             ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

    </head>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/trayindisponible">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area" style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2>Film list</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Tray /</a>
                            </li>
                            <li>
                                Actors
                            </li>
                        </ol>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#popup_6" class=" btn view-speaker ts-image-popup" data-effect="mfp-zoom-in"><i
                                        class="fa fa-plus"></i>&nbsp; New Film</a>
                                <!-- popup start-->
                                <div id="popup_6" class="container ts-speaker-popup mfp-hide">
                                    <div class="row">
                                        <div class="col-lg-6" style="background-size: cover;background-image: url(${pageContext.request.contextPath}/ressources/images/blog/istockphoto-1191001701-612x612.jpg)">
                                        </div><!-- col end-->
                                        <div class="col-lg-6">
                                            <div class="ts-speaker-popup-content">
                                                <form action="new-film">
                                                    <h3 class="ts-title">Nom Film</h3>
                                                    <input type="text" name="nomfilm" style="width: 100%;border-color: #d0d6db;border-top: none;border-left: none;border-right: none"/>
                                                    <p></p>
                                                    <h3 class="ts-title">Director name</h3>
                                                    <input type="text" name="nomrealisateur" style="width: 100%;border-color: #d0d6db;border-top: none;border-left: none;border-right: none"/>
                                                    <p></p>
                                                    <h3 class="ts-title">Description</h3>
                                                    <textarea name="descriptionfilm"  rows="4" class="form-control" style="border-left: none;border-right: none" placeholder="Film description"></textarea>
                                                    <p></p>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="speaker-session-info">
                                                                <h4>Picture cover</h4>
                                                                <input type="file" class="form-control" name="imagefilm" style="border:none"/>                                                                
                                                            </div>
                                                        </div>                                                        
                                                    </div>
                                                    <p></p>
                                                    <div style="text-align: center">                                                        
                                                        <button type="submit" class="btn">Save</button>
                                                    </div>
                                                    <p></p>
                                                    <div class="ts-speakers-social">
                                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                                    </div>
                                                </form>
                                            </div><!-- ts-speaker-popup-content end-->
                                        </div><!-- col end-->
                                    </div><!-- row end-->
                                </div><!-- popup end-->
                            </li>
                        </ol>
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->
            <!-- ts map direction start-->
            <section class="ts-map-direction wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="section-title">
                                <span>Info</span>
                                Film in production
                            </h2>
                        </div><!-- col end-->
                    </div><!-- row end-->
                    <div class="row">

                        <%
                            List<Film> film = (List<Film>) request.getAttribute("film");

                            for (Film elem : film) {
                        %>

                        <div class="col-lg-4 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                            <div class="post">
                                <div class="post-media post-image">
                                    <a href="#"><img src="${pageContext.request.contextPath}/ressources/images/blog/<%= elem.getImagefilm()%>" class="img-fluid" alt=""></a>
                                </div>

                                <div class="post-body">                                
                                    <div class="entry-header">
                                        <h2 class="entry-title">
                                            <a href="#"><%= elem.getNomfilm()%></a>
                                        </h2>
                                    </div><!-- header end -->
                                    <div class="post-meta">
                                        <span class="post-author">
                                            <a href="#">Director &nbsp;: </a>

                                            <a href="#"> <%= elem.getNomrealisateur()%> </a>
                                        </span>
                                    </div>
                                    <div class="entry-content">
                                        <p>Description film : <%= elem.getDescriptionfilm()%> </p>
                                    </div>

                                    <div class="post-footer">
                                        <a href="formulairescene?idfilm=<%=elem.getIdfilm()%>" class="btn-link">Create scene <i
                                                class="icon icon-camera-video"></i></a>
                                        <a href="" class="btn-link float-right">Read More <i
                                                class="icon icon-arrow-right"></i></a>
                                    </div>

                                </div><!-- post-body end -->
                            </div><!-- post end-->
                        </div><!-- col end-->
                        <p></p>
                        <%
                            }
                        %>

                    </div><!-- row end-->
                </div><!-- col end-->
                <!-- shap image-->
                <div class="speaker-shap">
                    <img class="shap2" src="images/shap/news_memphis2.png" alt="">
                    <img class="shap1" src="images/shap/news_memphis1.png" alt="">
                </div>
        </div><!-- container end-->
        <div class="speaker-shap">
            <img class="shap1" src="${pageContext.request.contextPath}/ressources/images/shap/Direction_memphis3.png" alt="">
            <img class="shap2" src="${pageContext.request.contextPath}/ressources/images/shap/Direction_memphis2.png" alt="">
            <img class="shap3" src="${pageContext.request.contextPath}/ressources/images/shap/Direction_memphis4.png" alt="">
            <img class="shap4" src="${pageContext.request.contextPath}/ressources/images/shap/Direction_memphis1.png" alt="">
        </div>
    </section>
    <!-- ts map direction end-->







    <!-- Javascript Files
            ================================================== -->
    <!-- initialize jQuery Library -->
    <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>

    <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
    <!-- Bootstrap jQuery -->
    <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
    <!-- Counter -->
    <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
    <!-- Countdown -->
    <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
    <!-- magnific-popup -->
    <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
    <!-- carousel -->
    <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
    <!-- Waypoints -->
    <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
    <!-- isotop -->
    <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>

    <!-- Template custom -->
    <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>

</div>
<!-- Body inner end -->
</body>


<!-- venue17:31-->

</html>