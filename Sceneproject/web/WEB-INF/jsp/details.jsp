<%@page import="model.Vueacteurscene"%>
<%@page import="java.util.List"%>
<%
    List<Vueacteurscene> vueacteurscene = (List) request.getAttribute("detail");

%>

<!DOCTYPE html>
<html lang="en">


    <!-- schedule-tab-217:31-->

    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Exhibit - Conference &amp; Event HTML Template</title>

        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">


    </head>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/trayindisponible">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area" style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2>Scene details</h2>                        
                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->
            <!-- ts schedule start-->
            <section class="ts-schedule">
                <div class="container">
                    <div class="row">
                        <%                            if (!vueacteurscene.isEmpty()) {
                        %>
                        <div class="col-lg-8 mx-auto">

                            <h2 class="section-title">

                                <%= vueacteurscene.get(0).getNomscene()%>

                                <p></p>
                                <span><%= vueacteurscene.get(0).getNomplateau()%></span>
                                <a class="btn-sm text-sm-center" style="height: 25px; text-align-last: center" href="${pageContext.request.contextPath}/scenes">Return</a>
                            </h2>                            
                            <div class="ts-schedule-nav">
                                <ul class="nav nav-tabs justify-content-center" role="tablist">
                                    <li class="nav-item">
                                        <a class="active" title="Click Me" href="#date1" role="tab" data-toggle="tab">
                                            Date
                                            <h3><%= vueacteurscene.get(0).getDatescene()%></h3>                                            
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- col end-->
                        <% } else {%>
                        <div class="col-lg-8 mx-auto">

                            <h2 class="section-title">

                                Pas de Details

                                <p></p>
                                <span>Pas de plateau</span>
                                <a class="btn-sm text-sm-center" style="height: 25px; text-align-last: center" href="${pageContext.request.contextPath}/scenes">Return</a>
                            </h2>                            
                            <div class="ts-schedule-nav">
                                <ul class="nav nav-tabs justify-content-center" role="tablist">
                                    <li class="nav-item">
                                        <a class="active" title="Click Me" href="#date1" role="tab" data-toggle="tab">
                                            Date
                                            <h3></h3>                                            
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- col end-->
                        <% }
                        %>
                    </div><!-- row end-->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content schedule-tabs schedule-tabs-item">
                                <div role="tabpanel" class="tab-pane active" id="date1">
                                    <% for (int i = 0; i < vueacteurscene.size(); i++) {%> 
                                    <div class="row">                                    
                                        <%if (i % 2 == 0) {
                                        %>
                                        <div class="col-lg-6">
                                            <div class="schedule-listing-item schedule-left" style="margin-top: 0px; padding-top: 0px">
                                                <img class="schedule-slot-speakers" src="${pageContext.request.contextPath}/ressources/images/image/<%= vueacteurscene.get(i).getPhotoacteur()%>" alt="">
                                                <span class="schedule-slot-time"><%= vueacteurscene.get(i).getDuree()%></span>
                                                <h3 class="schedule-slot-title"><%= vueacteurscene.get(i).getNomacteur()%></h3>                                                
                                                <h4 class="schedule-slot-name">script n� <%=i%></h4>
                                                <p>
                                                    <%= vueacteurscene.get(i).getScript()%>
                                                </p>
                                            </div>
                                        </div><!-- col end-->

                                    </div><!-- row end-->
                                    <% } else {%>
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-6">
                                        <div class="schedule-listing-item schedule-right" style="padding-top: 0px;">
                                            <img class="schedule-slot-speakers" src="${pageContext.request.contextPath}/ressources/images/image/<%= vueacteurscene.get(i).getPhotoacteur()%>" alt="">
                                            <span class="schedule-slot-time"><%= vueacteurscene.get(i).getDuree()%></span>
                                            <h3 class="schedule-slot-title"><%= vueacteurscene.get(i).getNomacteur()%></h3>                                                
                                            <h4 class="schedule-slot-name">script n� <%=i%></h4>
                                            <p>
                                                <%= vueacteurscene.get(i).getScript()%>
                                            </p>
                                        </div>
                                    </div><!-- col end-->
                                </div><!-- row end-->
                                <%
                                    }%>

                                <%}
                                %>

                            </div>
                        </div><!-- tab pane end-->
                    </div>

                </div>
        </div>
    </div><!-- container end-->
</section>

<script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>

<script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>

<script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>

</body>


<!-- schedule-tab-217:31-->

</html>