<%@page import="model.*"%>
<%@page import="java.util.List"%>
<%
    List<Indisponibiliteplateau> listeindispo = (List) request.getAttribute("listeindisponible");

%>
<!DOCTYPE html>
<html lang="en">


    <!-- schedule-list17:31-->

    <head>
        <!-- Basic Page Needs ================================================== -->
        <meta charset="utf-8">

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- Site Title -->
        <title>Exhibit - Conference &amp; Event HTML Template</title>

        <!-- CSS
             ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/bootstrap/css/bootstrap.min.css">

        <!-- FontAwesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/font-awesome.min.css">
        <!-- Animation -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/animate.css">
        <!-- magnific -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/magnific-popup.css">
        <!-- carousel -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/owl.carousel.min.css">
        <!-- isotop -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/isotop.css">
        <!-- ico fonts -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/xsIcon.css">
        <!-- Template styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/style.css">
        <!-- Responsive styles-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/ressources/css/responsive.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

    </head>

    <body>
        <div class="body-inner">
            <!-- Header start -->
            <header id="header" class="header header-transparent">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- logo-->
                        <a class="navbar-brand" href="index-2.html">
                            <img src="${pageContext.request.contextPath}/ressources/images/logos/logo.png" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}" class="">Home </a>
                                </li>
                                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/scene" class="">Create a scene</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" class="" data-toggle="dropdown">List <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="${pageContext.request.contextPath}/scenes" class="">Scene List</a></li>
                                        <li><a href="${pageContext.request.contextPath}/trayindisponible">Tray Unavailable</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- container end-->
            </header>
            <!--/ Header end -->

            <div id="page-banner-area" class="page-banner-area" style="background-image:url(${pageContext.request.contextPath}/ressources/images/banner/leanr_bg.png)">
                <!-- Subpage title start -->
                <div class="page-banner-title">
                    <div class="text-center">
                        <h2> List Tray Unavailable</h2>
                        <ol class="breadcrumb">
                            <li>
                                Tray unavailable for some reasons.
                            </li>
                        </ol>
                        <ol class="breadcrumb">
                            <li>
                                <button class="btn" data-toggle="modal" data-target="#addnew"><i
                                        class="fa fa-plus"></i>&nbsp; New</button>
                            </li>
                        </ol>

                    </div>
                </div><!-- Subpage title end -->
            </div><!-- Page Banner end -->

            <section class="ts-schedule">
                <div class="container">
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <h2 class="section-title">
                                For new planning
                            </h2>
                        </div>
                    </div>row end -->
                    <div class="row">

                        <!-- ito no bouclena -->
                        <% for (Indisponibiliteplateau indisponible : listeindispo) {%>

                        <div class="col-lg-4 pt-3">
                            <div class="single-intro-text single-contact-feature">
                                <h3 class="ts-title"><%=indisponible.getPlateauindispo().getNomplateau()%></h3>
                                <p>
                                    <strong>Reasons:</strong><%=indisponible.getMotifindispo()%>
                                </p>
                                <p>
                                    <strong>Date:</strong> <%=indisponible.getDateindispo()%>
                                </p>
                                <!-- <p>
                                    <strong>Scene:</strong> Scene name
                                </p> -->
                                <span class="count-number fa fa-film"></span>
                            </div>
                            <div class="border-shap left"></div>
                        </div>

                        <%}
                        %>
                        <!-- mihidy eto -->

                    </div><!-- row end-->

                </div><!-- container end-->
                <div class="speaker-shap">
                    <img class="shap2" src="images/shap/home_schedule_memphis1.png" alt="">
                    <img class="shap1" src="images/shap/home_schedule_memphis2.png" alt="">
                </div>
            </section>

            <div id="addnew" class="modal fade" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Tray Unavailable</h4><button class="btn-close" type="button" data-dismiss="modal"
                                                             aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <% List<Plateau> plateaus = (List<Plateau>) request.getAttribute("plateau"); %>
                            <form action="createindispo" method="post">
                                <div class="form-group mb-3">
                                    <label for="select">Choice Tray</label>
                                    <div class="input-group">
                                        <span class="text-primary input-group-text">
                                            <i class="fa fa-file-movie-o"></i>
                                        </span>
                                        <!-- <input class="form-control" type="date" required placeholder="Start date" /> -->
                                        <select class="form-control" id="select" name="idplateau">
                                            <% for (Plateau plateau : plateaus) {%>
                                            <option value="<%= plateau.getIdplateau()%>"><%= plateau.getNomplateau()%></option>
                                            <%   }%>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="date">Date</label>
                                    <div class="input-group"><span class="text-primary input-group-text"><i
                                                class="fa fa-calendar-times-o"></i></span><input class="form-control"
                                                                                         type="date" required placeholder="End date" id="date" name="dateindispo"/></div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="desc">Reasons</label>
                                    <div class="input-group">
                                        <span class="text-primary input-group-text">
                                            <i class="fa fa-text-height"></i>
                                        </span>
                                        <textarea class="form-control" id="desc" type="text" name="motifindispo" required
                                                  placeholder="Planned in filming"></textarea>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <button class="btn btn-sm" style="width: 100%;" type="submit">Ok</button>
                                </div>
                            </form>
                            <hr style="background-color: #bababa;" />
                        </div>
                    </div>
                </div>
            </div>


            <script src="${pageContext.request.contextPath}/ressources/js/jquery.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/popper.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.appear.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.jCounter.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/jquery.magnific-popup.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/owl.carousel.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/wow.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/isotope.pkgd.min.js"></script>
            <script src="${pageContext.request.contextPath}/ressources/js/main.js"></script>
        </div>
    </body>

</html>